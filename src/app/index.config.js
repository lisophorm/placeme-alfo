(function () {
  'use strict';

  angular
    .module('fuse')
    .config(config);

  /** @ngInject */
  function config(config, $provide,$httpProvider,AnalyticsProvider,$linkedInProvider,$urlRouterProvider,$mdDateLocaleProvider) {
    console.log('running config');
    console.log(config);

    //if($rootScope.isAdmin) {
    //  $urlRouterProvider.otherwise('/pages/search');
    // } else {
    //  $urlRouterProvider.otherwise('/pages/profile');
    // }

    $mdDateLocaleProvider.formatDate = function(date) {
      return moment(date).format('DD/MM/YYYY');
    };

    $mdDateLocaleProvider.parseDate = function(dateString) {
      var m = moment(dateString, 'DD/MM/YYYY', true);
      return m.isValid() ? m.toDate() : new Date(NaN);
    };

    $linkedInProvider
      .set('appKey', '77c0sh4voemltg')
      .set('scope', 'r_basicprofile r_emailaddress')
      .set('authorize', true);

    $provide.factory('myHttpInterceptor', function($q) {
      return {
        // optional method
        'request': function(config) {

          return config;
        },

        // optional method
        'requestError': function(rejection) {

          //if (canRecover(rejection)) {
          //    return responseOrNewPromise
          //  }
          console.log('********** intercepto erroir REQUEST');
          console.log(rejection);
          return $q.reject(rejection);
        },



        // optional method
        'response': function(response) {

          return response;
        },

        // optional method
        'responseError': function(rejection) {

          //if (canRecover(rejection)) {
          // return responseOrNewPromise
          // }
          console.log('********** intercepto erroir RESPONSE');
          console.log(rejection);
          return $q.reject(rejection);
        }
      };
    });
    //  $httpProvider.interceptors.push('myHttpInterceptor');
// Set a single account with all properties defined
// Universal Analytics only
    AnalyticsProvider.setAccount({
      tracker: 'UA-70752479-1',
      name: 'HybridTracker01',
      crossDomainLinker: true,
      displayFeatures: true,
      enhancedLinkAttribution: true,
      select: function (args) {
        if(window.location.hostname=='localhost') {
          return false;
        } else {
          return true;
        }
      },
      set: {
        forceSSL: true
        // This is any set of `set` commands to make for the account immediately after the `create` command for the account.
        // The property key is the command and the property value is passed in as the argument, _e.g._, `ga('set', 'forceSSL', true)`.
        // Order of commands is not guaranteed as it is dependent on the implementation of the `for (property in object)` iterator.
      },
      trackEvent: true
      //trackEcommerce: true
    });

    // Track all routes (default is true).
    AnalyticsProvider.trackPages(true);

    // Track all URL query params (default is false).
    AnalyticsProvider.trackUrlParams(true);

    // Ignore first page view (default is false).
    // Helpful when using hashes and whenever your bounce rate looks obscenely low.
    AnalyticsProvider.ignoreFirstPageLoad(true);

    AnalyticsProvider.setPageEvent('$stateChangeSuccess');
    AnalyticsProvider.setHybridMobileSupport(true);
  }

})();
