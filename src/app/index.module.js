(function () {
  'use strict';

  /**
   * Main module of the Fuse
   */
  angular
    .module('fuse', [

      // Core
      'app.core',

      // Navigation
      'app.navigation',

      // Toolbar
      'app.toolbar',

      // Quick panel
      'app.quick-panel',

      // Sample
      'app.sample',

      'app.pages.profile',

      'app.landing-candidate',
      'app.pages.auth.register',
      'app.pages.auth.login',
      'app.pages.sign-candidate',
      'app.pages.awaiting-approval'
    ]);
})();
