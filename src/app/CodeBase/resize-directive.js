'use strict';
/**
 * @ngdoc directive
 * @name fuse.directive:resize
 * @description
 * # resize
 */
angular.module('fuse')
  .directive('resize', function () {
    return function (scope, element, attr) {

      var w = angular.element($window);
      scope.$watch(function () {
        return {
          'h': window.innerHeight,
          'w': window.innerWidth
        };
      }, function (newValue, oldValue) {
        console.log(newValue, oldValue);
        scope.windowHeight = newValue.h;
        scope.windowWidth = newValue.w;

        scope.resizeWithOffset = function (offsetH) {
          scope.$eval(attr.notifier);
          return {
            'width': (newValue.w-50) + 'px',
            'height': (newValue.h - offsetH) + 'px',
            'overflow':'scroll'
          };
        };

      }, true);

      w.bind('resize', function () {
        scope.$apply();
      });
    };
  });
