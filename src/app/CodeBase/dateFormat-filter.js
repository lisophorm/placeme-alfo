'use strict';
/**
 * @ngdoc filter
 * @name fuse.filter:dateFormat
 * @function
 * @description
 * # dateFormat
 * Filter in the fuse.
 */
angular.module('fuse')
  .filter('dateFormat', function () {
    return function (input) {
      return 'dateFormat filter: ' + input;
    };
  });
