(function () {
    'use strict';
    angular.module('fuse').factory('GrabLnkdn', GrabLnkdn);


    function GrabLnkdn($cookies, config, $cookieStore, $http, $rootScope, $q, $mdToast, $mdDialog, AppUtil) {


        var service = {
            grabURL: grabURL
        };

        return service;

        function grabURL(url,parseObj) {
            var deferred = $q.defer();
            console.log("***************LINKEDIN" + url);
            Parse.Cloud.run('grabURL', {requestedUrl: url}, {
                success: function (reso) {
                    console.log("***************LINKEDIN");
                    profile("", reso,parseObj).then(function (msg) {
                        deferred.resolve("Your profile has been fetched successfully!");
                    }, function (msg) {
                        deferred.reject("Error fetching the profile");
                    });


                },
                error: function (error) {
                    deferred.reject(error);
                    console.log(error);
                }
            });
            return deferred.promise;

        }

        function profile(url, html,parseObj) {

            var user = parseObj;
            var school = [];
            var Schools = Parse.Object.extend('Schools');
            var Languages = Parse.Object.extend('Languages');
            var language = [];
            var WorkHistory = Parse.Object.extend('WorkHistory');
            var workHistory = [];
            var Clients = Parse.Object.extend('Clients');
            var clients = [];
            var obj = {};
            var clientObj = {};
            var inc = 0;
            var successSuck = true;

            var objectsBall = [];
            var clientsBall = [];

            var pattern = /<main[^>]*>((.|[\n\r])*)<\/main>/im
            var array_matches = pattern.exec(html);
            var errorMSG = "";
            var deferred = $q.defer();
            //var tmpsk=JSON.stringify(user.get("skilltags"));
          //user.set('skilltags',tmpsk);
            objectsBall.push(user);

            //console.log(array_matches[1]);
            $("#dummy").html($.parseHTML(array_matches[1]));
            var _JQ = $("#dummy"); // use Server-Side JQuery to access DOM
            var data = {url: url};    // store all parsed data inside data object

            var i = 0;                  // we re-use this in all our for loops below
            var id;                     // id of DOM node being searched
            var exp = {};               // experience object
            var h5;                     // in experience there are several h5 tags
            var date;                   // dates in profile e.g: when job started/ended


            data.fullname = $('.fn').text();
            data.location = $('#demographics .locality').text();
            user.set('location', data.location);
            data.current = $('.profile-overview-content p.headline').text().replace(/current/ig, '').trim();
            if (data.current.length < 3) {
                data.current = ''; // who has a 3-letter job?! exactly! leave it blank!
            }
            // console.log('>>>>>>> Current: '+data.current);
            var img = $('.profile-picture img');
            if (img[0]) {
                console.log("************* PROFILE PICTURE:" + img[0]);
                console.log(img[0]);
                //console.log(img[0].data());
                //data.picture = img[0]['attribs']['data-delayed-url'];
            }
            data.summary = $('#summary .description').text(); // element always present on page
            user.set('jobtitle', data.current);
            user.set('biography', data.summary);
            //var skills = $('.skill span ');


            data.skills = [];
            $(".skill span ").each(function (index) {
                console.log(index + ": " + $(this).text());
                data.skills.push({skill: $(this).text(), rating: 1});
                user.addUnique("skilltags", $(this).text());
            });
            user.set('skillset', angular.copy(data.skills));

            var langs = $('.language .wrap');

            data.languages = [];
            $(".language .wrap").each(function (index) {

                language[index] = obj = new Languages();
                objectsBall.push(obj);

                console.log(index + ": " + $(this).find('.name').html());
                console.log(index + ": " + $(this).find('.proficiency').html());
                obj.set('userID', parseObj.get('userID'));
                obj.set('name', $(this).find('.name').html());
                obj.set('proficiency', $(this).find('.proficiency').html());
                console.log("************* SAVING LANGUAGE");

            });

            var profilePic = $('.profile-picture img');
            profilePic.each(function (index) {
                console.log("************* PROFILE PICTURE" + index);

                try {
                    var urlPic = $(this).data().delayedUrl;
                    user.set('profileImgURL', urlPic);
                    user.unset('profileImgBin');
                } catch (e) {
                    console.log("************* PROFILE NOT PRESENT");
                }
            });


            //data.experience = {current:[], past:[]}; // empty if none listed
            var experience = [];
            var positions = $('#experience .position');
            positions.each(function (index) {

                console.log($(this).html());





                workHistory[index] = obj = new WorkHistory();
                objectsBall.push(obj);
                obj.set('userID', parseObj.get('userID'));
                obj.set('workRole', ($(this).find('.item-title a').html() !== undefined ? $(this).find('.item-title a').html() : $(this).find('.item-title').html()));
                obj.set('companyName', ($(this).find('.item-subtitle a').html() !== undefined ? $(this).find('.item-subtitle a').html() : $(this).find('.item-subtitle').html()));

                var companyName=($(this).find('.item-subtitle a').html() !== undefined ? $(this).find('.item-subtitle a').html() : $(this).find('.item-subtitle').html());


                obj.set('description', $(this).find('.description').html());
                obj.set('position', $(this).find('.position').html());
                obj.set('location', $(this).find('.location').html());


                clients[index] = clientObj = new Clients();
                clientsBall.push(clientObj);
                clientObj.set('name',companyName);


                var companyPic = $(this).find('.logo').find('img');
                companyPic.each(function (index) {
                    console.log("************* COMPANY PICTURE" + index);
                    console.log($(this).html());
                    try {
                        console.log($(this).data().delayedUrl);
                        var urlPic = $(this).data().delayedUrl;
                        obj.set('logoUrl', urlPic);
                        clientObj.set('logoUrl', urlPic);

                    } catch (e) {
                        console.log("************* IMAGE NOT PRESENT");
                    }
                });

                var companyURL = $(this).find('.item-subtitle a');
                console.log(companyURL);
                //console.log(companyURL.attr('href'));
                companyURL.each(function (index) {
                    console.log("************* COMPANY LINK" + index);
                    console.log($(this).html());
                    console.log($(this).text());
                    try {
                        console.log($(this).attr('href'));
                       // var urlPic = $(this).data().delayedUrl;
                        //obj.set('logoUrl', urlPic);
                        clientObj.set('link', $(this).attr('href'));
                        //clientObj.set('logoUrl', urlPic);

                    } catch (e) {
                        console.log("************* URL COMPANY NOT PRESENT");
                    }
                });

                console.log(index + ": " + $(this).html());

                //console.log(index + ": " + ($(this).find('.item-title a').html() !== undefined ? $(this).find('.item-title a').html() : $(this).find('.item-title').html()));

                //console.log(index + ": " + ($(this).find('.item-subtitle a').html() !== undefined ? $(this).find('.item-subtitle a').html() : $(this).find('.item-subtitle').html()));
                console.log(index + ": " + $(this).find('.date-range').html());
                //console.log(index + ": " + $(this).find('.location').html());
                console.log(index + ": " + $(this).find('time').length);
                //console.log(index + ": " + $(this).find('.description').html());
                //console.log(index + ": " + $(this).find('.position').html());

                data.time = [];
                $(this).find('time').each(function (index) {
                    data.time[index] = $(this).html();
                });
                var is1 = /^\d{1,4}$/.test(data.time[0]);
                var is2 = /^\d{1,4}$/.test(data.time[1]);
                if(is1) {
                    data.time[0]=" 01 "+data.time[0];
                }
                if(is2) {
                    data.time[1]=" 01 "+data.time[1];
                }
                console.log("******** ORIGINAL BEGIN DATE:" + data.time[0]);
                var bgDate=new Date("1 " + data.time[0]);
                if(typeof bgDate.getMonth === 'function') {
                    obj.set('beginDate', bgDate);
                } else {
                    console.log("******** ERROR BEGIN DATE:" + bgDate);
                }
                console.log("******** actual BEGIN DATE:" + bgDate);
                //console.log(obj.get('beginDate'));
                try {
                    console.log("******** ORIGINAL END DATE" + data.time[1]);
                    var endDate = new Date("1 " + data.time[1]);
                    console.log("******** FORMATTING END DATE:" + endDate);
                    if (typeof data.time[1] !== 'undefined') {
                        console.log("******** END DATE RIGHT:" +endDate);
                        obj.set('endDate', endDate);
                    } else {
                        console.log("******** DATE UNDEFINED "+endDate);
                        obj.set('endDate', new Date('3000-01-01'));
                    }

                } catch (e) {
                    obj.set('endDate', new Date('3000-01-01'));

                }

                //console.log(obj.get('endDate'));


                data.dateRange = $(this).find('.date-range').html().toLowerCase();
                if (data.dateRange.indexOf('present') > -1) {
                    obj.set('currentWork', true);
                } else {
                    obj.set('currentWork', false);
                }


                //data.languages = [{
                // name: $( this).find('.name').html() ,
                //proficiency: $( this).find('.proficiency').html()
                // }];
            });
            Parse.Object.saveAll(clientsBall);


            var educations = $('#education .school');


            // process school
            inc = 0;
            console.log("************* BEFORE EDUCATION");
            educations.each(function (index) {
                console.log("************* WITHIN EDUCATION " + index);
                console.log($(this).html());
                school[index] = obj = new Schools();
                objectsBall.push(obj);
                obj.set('userID', parseObj.get('userID'));
                obj.set('title', ($(this).find('.item-title a').html() !== undefined ? $(this).find('.item-title a').html() : $(this).find('.item-title').html()));
                obj.set('subtitle', ($(this).find('.item-subtitle a').html() !== undefined ? $(this).find('.item-subtitle a').html() : $(this).find('.item-subtitle').html()));

                console.log(index + ": " + $(this).html());

                console.log(index + ": " + ($(this).find('.item-title a').html() !== undefined ? $(this).find('.item-title a').html() : $(this).find('.item-title').html()));
                console.log(index + ": " + ($(this).find('.item-subtitle a').html() !== undefined ? $(this).find('.item-subtitle a').html() : $(this).find('.item-subtitle').html()));

                console.log(index + ": " + $(this).find('time').length);
                var schoolrange = $(this).find('time');
                console.log(schoolrange);
                console.log(schoolrange.eq(0).html());
                console.log(schoolrange.eq(1).html());
                obj.set('beginTime', schoolrange.eq(0).html());
                obj.set('endTime', schoolrange.eq(1).html());
                console.log("************* SAVING SCHOOL");

                console.log($(this).find('.date-range').children());

            });

            var query = new Parse.Query(WorkHistory);
            query.equalTo('userID',parseObj.get('userID'));
            query.find().then(function (result) {
                AppUtil.showSuccess("Deleted old Work History");
                return Parse.Object.destroyAll(result);
            }, function (error) {
                AppUtil.showErr(JSON.stringify(error.message));
            }).then(function (result) {
                var query = new Parse.Query(Schools);
                query.equalTo('userID', parseObj.get('userID'));
                return query.find()
            }).then(function (result) {
                return Parse.Object.destroyAll(result);
            }, function (error) {
                AppUtil.showErr(JSON.stringify(error.message));
            }).then(function (result) {
                AppUtil.showSuccess("Deleted old Schools");
                var query = new Parse.Query(Languages);
                query.equalTo('userID', parseObj.get('userID'));
                return query.find();

            }, function (error) {
                AppUtil.showErr(error.message);
            }).then(function (result) {
                    AppUtil.showSuccess("Deleted old Languages");
                    console.log("************* TUTTA LA MADONNA SALVATA");
                    return Parse.Object.saveAll(objectsBall);

                },
                function (error) {
                    AppUtil.showErr(error.message);
                }).then(function (result) {
                    AppUtil.showSuccess("Linkedin profile saved!");
                    console.log("************* TUTTA LA MADONNA SALVATA");
                    deferred.resolve(result);
                },
                function (error) {
                    console.log("************* TUTTA LA MADONNA WERROR");
                    console.log(error);
                    deferred.reject(error);
                });
            console.log("********** FINE");
            console.log('************** FINO A QUA');
            console.log(JSON.stringify(data));
            return deferred.promise;
        }


    }
})();
