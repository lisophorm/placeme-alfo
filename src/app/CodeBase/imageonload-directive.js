'use strict';
/**
 * @ngdoc directive
 * @name fuse.directive:imageonload
 * @description
 * # imageonload
 */
angular.module('fuse')
  .directive('imageonload', function () {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.bind('load', function() {
          console.log('image is loaded');
          console.log(element[0].width);
          var prop=element[0].naturalWidth/element[0].naturalHeight;
          console.log("prop="+prop);
          var alto=element[0].width/prop;
          console.log("alto:"+alto);
          element[0].height=alto;
        });
        element.bind('error', function(){
          console.log('image could not be loaded');
        });
      }
    };
  });
