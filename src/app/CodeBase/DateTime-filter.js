'use strict';
/**
 * @ngdoc filter
 * @name fuse.filter:datetime
 * @function
 * @description
 * # datetime
 * Filter in the fuse.
 */
angular.module('fuse')
  .filter('dateTime', function () {
    return function(input)
    {
      if(input == null){ return ""; }

      var _date = $filter('date')(new Date(input), 'MMM dd yyyy');

      return _date.toString();

    };
  });
