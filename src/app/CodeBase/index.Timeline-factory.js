(function () {
  'use strict';
  angular.module('fuse').factory('Timeline', Timeline)

  function Timeline($cookies, config, $cookieStore, $http, $rootScope, $q, $mdToast, $mdDialog, Analytics) {

    var service = {
      loadTimeline: loadTimeline,
      getEvent:getEvent,
      addEvent:addEvent,
      saveEvent:saveEvent
    };

    return service;
    this.access_token = '';
    this.parseObject = {};
    service.timeline = [];
    service.pointers = [];

    function loadTimeline(userID,scope) {

      console.log('called load timeline');
      var deferred = $q.defer();
      var myPointer = new Parse.Object("_User");
      myPointer.id = userID;
      var query = new Parse.Query('WorkHistory');
      query.equalTo("userID", myPointer);
      query.find().then(function(result) {
        service.timeline = [];
        service.pointers=[];
        for(var i=0;i<result.length;i++) {
          console.log("loop timeline"+i);
          var obj=result[i];
          var dato=makeObj(obj);
          dato.index=i;
          dato.id=obj.id;
          service.timeline.push(dato);
          service.pointers.push(obj);
        }
        console.log('timeline now');
        console.log(service.timeline);
        deferred.resolve(service.timeline);
      },function(error) {
        deferred.reject(error);
      })

      return deferred.promise;

    }

    function getEvent(index) {
      return angular.copy(service.timeline[index]);
    }

    function addEvent() {

    }

    function saveEvent(index) {

    }

    function makeObj(parseThing) {
      var keys = Object.keys(parseThing.toJSON());
      var obj = {};
      for (var a in keys) {


        var key = keys[a];

        if (parseThing.has(key)) {
          obj[key] = parseThing.get(key);
        }

      }
      obj.id = parseThing.id;
      console.log('oj');
      console.log(obj);
      return obj;
    }

  }
})();
