(function () {
  'use strict';
  angular.module('fuse').factory('UserService', UserService);


  function UserService($cookies, config, $cookieStore, $http, $rootScope, $q, AppUtil) {

    var parseInitialized = false;


    return {

      /**
       *
       * @returns {*}
       */
      init: function () {
        //return $q.reject({error: 'noUser'});
        var deferred = $q.defer();
        //debugger;
        // if initialized, then return the activeUser
        if (parseInitialized === false) {
          Parse.initialize(config.applicationId, config.javascriptKey);
          parseInitialized = true;
          console.log('parse initialized in init function');
        }

        var currentUser = Parse.User.current();
        console.log('****** USERSERVICE INIT');
        try {
          console.log(Parse.User.current().get('firstname'));

        } catch (e) {
          console.log('****** USERSERVICE  NOT INIT');
        }
        if (currentUser) {
          currentUser.fetch().then(function(reso) {
            Parse.Cloud.run('isAdmin',null,{success:function(resto) {
              console.log('****** USERSERVICE  ISADMIN');
              console.log(resto);
              $rootScope.isAdmin = resto.isAdmin;
              $rootScope.superAdmin = resto.superAdmin;
              $rootScope.roleType=resto.roleType;
              if($rootScope.isAdmin || $rootScope.superAdmin) {
                deferred.resolve(true);
              } else if(!reso.has('registrationStatus')) {
                console.log('****** USERSERVICE  REGO INCOMPLETE');
                deferred.reject({error: 'regoIncomplete'});
              } else if(reso.get('registrationStatus')=='submitted') {
                console.log('****** USERSERVICE  REGO SUBMITTED');
                deferred.reject({error: 'regoAwaiting'});
              } else if(reso.get('registrationStatus')=='approved') {
                deferred.resolve(true);
              } else {
                deferred.reject({error: 'regoIncomplete'});
              }
            },error:function(erroro) {
              console.log('****** USERSERVICE  ISADMIN ERRORO');
              console.log(erroro);
              deferred.reject({error: 'isAdminError'});
            }});
            console.log('****** USERSERVICE  FETCH');
            console.log(reso.get('registrationStatus'));
           /* if(!reso.has('registrationStatus')) {
              console.log('****** USERSERVICE  REGO INCOMPLETE');
              return deferred.reject({error: 'regoIncomplete'});
            } else if(reso.get('registrationStatus')=='submitted') {
              console.log('****** USERSERVICE  REGO SUBMITTED');
              return deferred.reject({error: 'regoAwaiting'});
            } else if(reso.get('registrationStatus')=='approved' && ($rootScope.isAdmin || $rootScope.superAdmin)) {
              return deferred.resolve(true);
            } else {
              return deferred.reject({error: 'regoIncomplete'});
            }*/
          },function(cacco) {
            console.log('****** USERSERVICE  FETCH ERROR');
          })
          //return $q.when(currentUser);
        } else {
          console.log('****** USERSERVICE  noUSER');
          deferred.reject({error: 'noUser'});
        }
        return deferred.promise;
      },
      /**
       *
       * @param _userParams
       */
      createUser: function (_userParams) {
        console.log(_userParams);

        var user = new Parse.User();
        user.set('username', _userParams.email);
        user.set('password', _userParams.password);
        user.set('email', _userParams.email);
        user.set('firstname', _userParams.firstname);
        user.set('lastname', _userParams.lastname);
        user.set('captchares', _userParams.captchares);

        // should return a promise
        return user.signUp(null, {});

      },
      /**
       *
       * @param _parseInitUser
       * @returns {Promise}
       */
      currentUser: function (_parseInitUser) {

        // if there is no user passed in, see if there is already an
        // active user that can be utilized
        $rootScope.daUser = _parseInitUser = _parseInitUser ? _parseInitUser : Parse.User.current();

        console.log('_parseInitUser ' + Parse.User.current());
        if (!_parseInitUser) {
          return $q.reject({error: 'noUser'});
        } else {
          return $q.when(_parseInitUser);
        }
      },
      /**
       *
       * @param _user
       * @param _password
       * @returns {Promise}
       */
      login: function (_user, _password) {

       return Parse.Cloud.run('loginCloud',{email:_user,pwd:_password});


        //return Parse.User.logIn(_user, _password);
      },
      /**
       *
       * @returns {Promise}
       */
      logout: function (_callback) {
        $cookies.remove('userStuff');
        var defered = $q.defer();
        Parse.User.logOut();
        defered.resolve();
        return defered.promise;

      }

    };

  }
})();
