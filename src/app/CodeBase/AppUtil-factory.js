(function () {
  'use strict';
  angular.module('fuse').factory('AppUtil', AppUtil);

  function AppUtil($cookies, config, $cookieStore, $http, $rootScope, $q, $mdToast, $mdDialog, Analytics,$window) {
    this.idleTimer = 0;

    var service = {
      showSuccess: showSuccess,
      showErr: showErr,
      randomString: randomString,
      showAlert: showAlert,
      scorePassword: scorePassword,
      checkPassStrength: checkPassStrength,
      storeUser: storeUser,
      compDates: compDates,
      triggerProgress: triggerProgress,
      startProgress: startProgress
    };

    return service;


    function startProgress() {
      $rootScope.loadingProgress = true;
      if ($rootScope.$$phase != '$apply' && $rootScope.$$phase != '$digest') {
        $rootScope.$apply();
      }
    }

    function triggerProgress() {
      if ($rootScope.loadingProgress == false) {
        $rootScope.loadingProgress = true;
      }
      clearTimeout(this.idleTimer);
      console.log('RESET PROGRESS');
      this.idleTimer = setTimeout(function () {


        console.log('CLOSE PROGRESS');
        $rootScope.loadingProgress = false;
        if ($rootScope.$$phase != '$apply' && $rootScope.$$phase != '$digest') {
          $rootScope.$apply();
        }


      }, 1000);
    }

    function randomString(len, charSet) {
      charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var rndString = '';
      for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        rndString += charSet.substring(randomPoz, randomPoz + 1);
      }
      return rndString;
    }

    function scorePassword(pass) {
      var score = 0;
      if (!pass)
        return score;

      // award every unique letter until 5 repetitions
      var letters = new Object();
      for (var i = 0; i < pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
      }

      // bonus points for mixing it up
      var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
      }

      var variationCount = 0;
      for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
      }
      score += (variationCount - 1) * 15;

      return parseInt(score);
    }

    function checkPassStrength(pass) {
      var score = scorePassword(pass);
      if (score > 80)
        return "strong";
      if (score > 60)
        return "good";
      if (score >= 30)
        return "weak";

      return "";
    }

    function storeUser(key, value) {
      var now = new Date();
      // this will set the expiration to 24 months
      var exp = new Date(now.getFullYear() + 2, now.getMonth(), now.getDate());

      var currentObj = ($cookies.getObject('userStuff') ? $cookies.getObject('userStuff') : {});

      currentObj[key] = value;

      $cookies.putObject('userStuff', currentObj, {
        expires: exp
      });


    }

    function cloudError(errorStr) {
      var response = "";
      try {
        response = JSON.parse(errorStr);
        return response;
      } catch (e) {
        return response;
      }
    }

    function compDates(starDate, endDate) {


      var begin = new XDate(starDate);


      var end = new XDate(endDate);

      var startX = new XDate(begin.getFullYear(), begin.getMonth(), begin.getDate());
      var endX = new XDate(end.getFullYear(), end.getMonth(), end.getDate());

      if (startX.toString() == endX.toString()) {
        console.log(startX + " - " + endX);
      }
      return startX.toString() == endX.toString();
    }

    function showAlert(title, msg) {
      // Appending dialog to document.body to cover sidenav in docs app
      // Modal dialogs should fully cover application
      // to prevent interaction outside of dialog
      $mdDialog.show(
        $mdDialog.alert()
          .parent(angular.element(document.body))
          .title(title)
          .content(msg)
          .ariaLabel(msg)
          .ok('Got it!')
        //.targetEvent(ev)
      );
    }

    function showErr(msg) {
      Analytics.trackEvent('toast', 'error', msg);
      //$window.trackJs.track(msg);
      console.log('tracking error');
      console.log(msg);
      console.log(typeof msg);
      var theMessage='';
      if (typeof msg === 'string') {
        theMessage=msg;
      } else {
        theMessage=msg.toString();
      }

      $mdToast.show({
        autoWrap: false,
        hideDelay: 15000,
        template: '<md-toast class="md-red-700-bg ph-8 pb-8"><div flex layout="row" layout-align="space-between center">\
        <div flex>{{ toast.content }}</div>\
        <md-button ng-click="toast.closeToast()" aria-label="Close">\
             CLOSE\
                 </md-button>\
    </div></md-toast>',
        controller: [function () {
          this.closeToast = function () {
            $mdToast.hide();
          };
          this.content = theMessage;
        }],
        controllerAs: 'toast'
      });
    }

    function showSuccess(msg) {
      $mdToast.show({
        template: '<md-toast class="md-green-600-bg ph-16 pb-16"><span flex>\
        {{ toast.content }}\
    </span></md-toast>',

        autoWrap: false,
        controller: [function () {
          if (msg.indexOf('XMLHttpRequest') != -1) {
            console.log('mlhttp');
            msg += ' - check your internet connection!';
          }
          this.closeToast = function () {
            $mdToast.hide();
          };
          this.content = msg;

        }],
        controllerAs: 'toast'
      });
    }

  }
})();
