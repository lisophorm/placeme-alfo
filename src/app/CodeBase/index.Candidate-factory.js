(function () {
  'use strict';
  angular.module('fuse').factory('Candidate', Candidate)

  function Candidate($cookies, config, $cookieStore, $http, $rootScope, $q, $mdToast, $mdDialog, Analytics) {

    var service = {
      serverData: serverData,
      loadCandidate: loadCandidate,
      saveCandidate: saveCandidate,
      uploadProfile: uploadProfile
    };

    return service;
    var serverData = {};
    this.access_token = '';
    this.parseObject = {};
    this.candidate = {};
    service.candidatePointer = {};

    function loadCandidate(userID) {
      var deferred = $q.defer();
      var myPointer = new Parse.Object("_User");
      myPointer.id = userID;
      var query = new Parse.Query('UserInfo');
      query.equalTo("userID", myPointer);
      query.first().then(function (reso) {
        service.candidatePointer = reso;
        console.log('candidate pointer');
        console.log(service.candidatePointer);
        serverData = makeObj(reso);
        deferred.resolve(serverData);
      }, function (erro) {
        deferred.reject(erro)
      });

      return deferred.promise;

    }

    function makeObj(parseThing) {
      var keys = Object.keys(parseThing.toJSON());
      var obj = {};
      for (var a in keys) {


        var key = keys[a];

        if (parseThing.has(key)) {
          obj[key] = parseThing.get(key);
        }

      }
      obj.id = parseThing.id;
      console.log('oj');
      console.log(obj);
      return obj;
    }

    function saveCandidate(candiData) {
      console.log('candidate pointer');
      console.log(service.candidatePointer);
      var deferred = $q.defer();
      angular.forEach(candiData, function (value, key) {
        service.candidatePointer.set(key, value);
        console.log(key + ': ' + value);
      });
      service.candidatePointer.save().then(function (successo) {
        deferred.resolve(successo);
      }, function (erroro) {
        deferred.reject(erroro);
      })
      return deferred.promise;
    }

    function uploadProfile(tadaFil) {
      var keepGoing = true;
      var deferred = $q.defer();
      var file = new Parse.File("profile.png", {base64: tadaFil});

      file.save().then(function (resulto) {
        // The file has been saved to Parse.
        console.log('file saved');
        console.log(file);
        console.log(resulto._url);
        //service.serverData.profileImgBin = file;
        //service.serverData.profileImgURL = resulto._url;
        service.candidatePointer.set('profileImgBin', file);
        service.candidatePointer.set('profileImgURL', resulto._url);
        return service.candidatePointer.save();

      }, function (error) {
        keepGoing = false;
        deferred.reject(error);
      }).then(function (obj) {
        if (keepGoing) {
          deferred.resolve( service.candidatePointer.get('profileImgURL'));
        }

      }, function (error) {
        if (keepGoing) {
          deferred.reject(error);
        }
      });
      return deferred.promise;
    };


  }
})();
