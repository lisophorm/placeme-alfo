'use strict';
/**
 * @ngdoc filter
 * @name fuse.filter:dateTimeFormat
 * @function
 * @description
 * # dateTimeFormat
 * Filter in the fuse.
 */
angular.module('fuse')
  .filter('dateTimeFormat', function () {
    return function(input)
    {
      if(input == null){ return ""; }

      var _date = $filter('date')(new Date(input), 'HH:mm - MMM dd yyyy');

      return _date.toString();

    };
  });
