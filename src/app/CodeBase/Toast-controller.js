'use strict';
/**
 * @ngdoc function
 * @name fuse.controller:toastCtrl
 * @description
 * # toastCtrl
 * Controller of the fuse
 */
angular.module('fuse')
  .controller('toastCtrl', function ($scope,$mdToast) {
    this.content = msg;
    $scope.closeToast = function () {
      $mdToast.hide();
    };
  });
