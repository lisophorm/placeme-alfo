(function () {
  'use strict';
  angular.module('fuse').factory('SigninHub', SigninHub);


  function SigninHub($cookies, config, $cookieStore, $http, $rootScope, $q, $mdToast, $mdDialog, Analytics, Upload,AppUtil) {


    var access_token = '';
    var document_id = '';
    var pdf_object_id = '';
    var total_document_pages=1;
    var service = {
      signToken: signToken,
      getPDF: getPDF,
      processTimesheet: processTimesheet,
      finalizeTimesheet: finalizeTimesheet,
      deleteTimesheet:deleteTimesheet,
      processDocument: processDocument
    };

    return service;



    function deleteTimesheet(timesheetID) {
      var deferred = $q.defer();
      Parse.Cloud.run('deleteSignDocument', {pdfID: timesheetID})
        .then(function (success) {
        console.log('KILLED TIMESHEET');
        access_token = success.access_token;
        return deferred.resolve('success');
      }, function (error) {
        console.log('CLOUD ERROR');
        return  deferred.reject(error);
      });;
      return deferred.promise;
    }

    function signToken() {
      var deferred = $q.defer();
      $rootScope.$broadcast("SignHubEvent", "Authenticate");
      Parse.Cloud.run('getSignToken').
      then(function (success) {
        console.log('CLOUD SUCCESS');
        access_token = success.access_token;
        deferred.resolve({'access_token': success.access_token});


      }, function (error) {
        console.log('CLOUD ERROR');
        return  deferred.reject(error);
      });

      return deferred.promise;
    }

    function getDoc(docID) {
      var deferred = $q.defer();
      $rootScope.$broadcast("SignHubEvent", "Authenticate");
      Parse.Cloud.run('getSignToken', {email: 'alfo@crystal-bits.co.uk', docID: 'NTzGdx68Q5'}).
      then(function (success) {
        console.log('CLOUD SUCCESS');
        access_token = success.access_token;
        deferred.resolve({'access_token': success.access_token});


      }, function (error) {
        console.log('CLOUD ERROR');
        return  deferred.reject(error);
      });

      return deferred.promise;
    }

    function getPDF(docID) {
      console.log('get PDF');
      pdf_object_id=docID;
      var deferred = $q.defer();
      console.log('run file');
      $rootScope.$broadcast("SignHubEvent", "Generate PDF");
      Upload.upload({
          url: 'http://bottle-crystalbits.rhcloud.com/pdf?url=app.hybridrecruitment.com/timesheet/' + docID,
          method: 'GET',
          headers: {'Content-type': 'application/x-www-form-urlencoded'},
          responseType: 'blob'
        }
      ).then(function (response) {
          console.log('bob blobbato');
          var blobbo = new Blob(new Array(response.data), {type : 'application/json'});
          //return deferred.resolve(new File(new Array(response.data), 'muyfile.pdf', {type: "application/pdf"}));
          return deferred.resolve(blobToFile(blobbo, 'muyfile.pdf'));
        }

        , function (error) {
          console.log('error');
          return deferred.reject(error);
        }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $rootScope.$broadcast("SignHubEvent", "Upload PDF "+ progressPercentage + '% ');
          console.log('progress: ' + progressPercentage + '% ' + vm.pdfFile.name);
        });

      return deferred.promise;


    }
    function blobToFile(theBlob, fileName){
      //A Blob() is almost a File() - it's just missing the two properties below which we will add
      theBlob.lastModifiedDate = new Date();
      theBlob.name = fileName;
      return theBlob;
    }
    function processTimesheet(docID, signerEmail, signerName,signerID) {
      pdf_object_id=docID;
      console.log('process timesheet');
      console.log(pdf_object_id);
      var deferred = $q.defer();
      signToken().then(function(succo) {
        $rootScope.$broadcast("SignHubEvent", "Validate Signer");
        return Parse.Cloud.run('signHubUser', {access_token:access_token,name:signerName,email:signerEmail});
      },function (erro) {
        $rootScope.$broadcast("SignHubError", erro);
        return deferred.reject(erro);
      }).then(function(succo) {
          $rootScope.$broadcast("SignHubEvent", "Generate PDF");
          return getPDF(docID);
      },function (erro) {
          $rootScope.$broadcast("SignHubError", erro);
          return deferred.reject(erro);
      })
      .then(function (response) {
          console.log('bob blobbato');
          $rootScope.$broadcast("SignHubEvent", "Upload PDF");
          return Upload.upload({
            url: 'https://api.signinghub.com/v2/documents',
            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data; boundary= ----FormBoundaryE19zNvXGzXaLvS5C',
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + access_token,
            },
            data: {file: response}
          })
        }

        , function (error) {
          console.log('error');
          $rootScope.$broadcast("SignHubError", error);
          return deferred.reject(error);
        }).then(function (resp) {
        console.log('Success upload ');
        console.log(resp);
        document_id = resp.data.document_id;
        $rootScope.$broadcast("SignHubEvent", "Apply Signature");
        return $http({
          method: 'POST',
          url: 'https://api.signinghub.com/v2/documents/' + document_id + '/workflow/template',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + access_token,
          },
          data: {'template_name': 'template02'}

        })
        //vm.docID=resp.data.document_id;
      }, function (resp) {
        console.log('Error status upload:');
        $rootScope.$broadcast("SignHubError", resp);
        return deferred.reject(resp);
        console.log(resp);
      }, function (evt) {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        console.log('progress: ' + progressPercentage + '% ');
      }).then(function(reso){
          $rootScope.$broadcast("SignHubEvent", "Assign document to Line Manager");
          console.log('https://api.signinghub.com/v2/documents/'+document_id+'/workflow/1/user');

          return $http({
            url:'https://api.signinghub.com/v2/documents/'+document_id+'/workflow/1/user',
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              'Accept':'application/json',
              'Authorization': 'Bearer '+access_token,
            },
            data: { 'user_email': signerEmail,
              'email_notification': false
            }

          })},
        function(erro1) {
          $rootScope.$broadcast("SignHubError", erro1);
          return deferred.reject(erro1);
        }).then(function (file) {
        console.log('asssigned urer');
        $rootScope.$broadcast("SignHubEvent", "Share Document");
        console.log('https://api.signinghub.com/v2/documents/'+document_id+'/workflow');
        return $http({
          method: 'POST',
          url:'https://api.signinghub.com/v2/documents/'+document_id+'/workflow',
          headers: {
            'Content-Type': 'application/json',
            'Accept':'application/json',
            'Authorization': 'Bearer '+access_token,
          },

        })
      }, function (error) {
        console.log('error assigning user');
        console.log(error);
        $rootScope.$broadcast("SignHubError", error);
        return deferred.reject(error);
       }).then(function(response) {
        console.log('document shared');
        console.log(response);
        $rootScope.$broadcast("SignHubEvent", "Emailing Timesheet");
        return Parse.Cloud.run('finalizeTimesheet', {pdfID: pdf_object_id,docID:document_id});
      },function(error) {
        $rootScope.$broadcast("SignHubError", error);
        console.log('error');
        return deferred.reject(error);
      }).then(function(response) {
        $rootScope.$broadcast("SignHubEvent", "complete");
        console.log('document shared');
        console.log(response);
        return deferred.resolve(response);
      },function(error) {
        $rootScope.$broadcast("SignHubError", error);
        console.log('error');
        return deferred.reject(error);
      });


      return deferred.promise;
    }

    function processDocument(theFile,signer1,signer2,contractTitle){
      console.log("signer 1");
      console.log(signer1);
      console.log('signer 2');
      console.log(signer2);
      var fieldnames=[];



        fieldnames[1]={field1:'SH_SIGNATURE_168650',field2:'SH_SIGNATURE_201900'};

      fieldnames[2]={field1:'SH_SIGNATURE_146018',field2:'SH_SIGNATURE_637542'};

      fieldnames[3]={field1:'SH_SIGNATURE_130945',field2:'SH_SIGNATURE_159949'};
      //"<FieldsResponse xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/AscertiaDocs.WebAPI.Models.V62"><form_fields /><in_persons /><initials /><signature_fields><SignatureField><embedded>false</embedded><field_name></field_name><height>34</height><mobile_number i:nil="true" /><order>1</order><page_no>4</page_no><placeholder>signer1</placeholder><process_status>UN_PROCESSED</process_status><processed_as i:nil="true" /><processed_by i:nil="true" /><processed_on>0001-01-01T00:00:00</processed_on><reason i:nil="true" /><sign_sms_otp>false</sign_sms_otp><signature_sub_type>NONE</signature_sub_type><signature_type>E_SIGNATURE</signature_type><type>PLACEHOLDER</type><verification i:nil="true" /><width>84</width><x>115</x><y>758</y></SignatureField><SignatureField><embedded>false</embedded><field_name></field_name><height>34</height><mobile_number i:nil="true" /><order>2</order><page_no>4</page_no><placeholder>signer2</placeholder><process_status>UN_PROCESSED</process_status><processed_as i:nil="true" /><processed_by i:nil="true" /><processed_on>0001-01-01T00:00:00</processed_on><reason i:nil="true" /><sign_sms_otp>false</sign_sms_otp><signature_sub_type>NONE</signature_sub_type><signature_type>E_SIGNATURE</signature_type><type>PLACEHOLDER</type><verification i:nil="true" /><width>84</width><x>432</x><y>757</y></SignatureField></signature_fields></FieldsResponse>"

      fieldnames[4]={field1:'SH_SIGNATURE_126851',field2:'SH_SIGNATURE_504090'};

      //"<FieldsResponse xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/AscertiaDocs.WebAPI.Models.V62"><form_fields /><in_persons /><initials /><signature_fields><SignatureField><embedded>false</embedded><field_name></field_name><height>34</height><mobile_number i:nil="true" /><order>1</order><page_no>5</page_no><placeholder>signer1</placeholder><process_status>UN_PROCESSED</process_status><processed_as i:nil="true" /><processed_by i:nil="true" /><processed_on>0001-01-01T00:00:00</processed_on><reason i:nil="true" /><sign_sms_otp>false</sign_sms_otp><signature_sub_type>NONE</signature_sub_type><signature_type>E_SIGNATURE</signature_type><type>PLACEHOLDER</type><verification i:nil="true" /><width>84</width><x>116</x><y>769</y></SignatureField><SignatureField><embedded>false</embedded><field_name></field_name><height>34</height><mobile_number i:nil="true" /><order>2</order><page_no>5</page_no><placeholder>signer2</placeholder><process_status>UN_PROCESSED</process_status><processed_as i:nil="true" /><processed_by i:nil="true" /><processed_on>0001-01-01T00:00:00</processed_on><reason i:nil="true" /><sign_sms_otp>false</sign_sms_otp><signature_sub_type>NONE</signature_sub_type><signature_type>E_SIGNATURE</signature_type><type>PLACEHOLDER</type><verification i:nil="true" /><width>84</width><x>418</x><y>769</y></SignatureField></signature_fields></FieldsResponse>"
      fieldnames[5]={field1:'SH_SIGNATURE_186302',field2:'SH_SIGNATURE_103022'};

      var deferred = $q.defer();
      $rootScope.$broadcast("SignHubEvent", "Authentication");
      signToken().then(function(succo) {
        $rootScope.$broadcast("SignHubEvent", "Validate Signer");
        return Parse.Cloud.run('signHubUser', {access_token:access_token,name:signer2.name,usedId:signer2.id,email:signer2.email});
      },function (erro) {
        $rootScope.$broadcast("SignHubError", erro);
        return deferred.reject(erro);
      }).then(function(response) {
        $rootScope.$broadcast("SignHubEvent", "Uploading document");
        console.log('document shared');
        console.log(response);
        return Upload.upload({
          url: 'https://api.signinghub.com/v2/documents',
          method:'POST',
          headers : {
            'Content-Type': 'multipart/form-data; boundary= ----FormBoundaryE19zNvXGzXaLvS5C',
            'Accept':'application/json',
            'Authorization': 'Bearer '+access_token
          },
          data: {file:theFile}
        }).then(function (resp) {
          console.log('Success ');
          console.log(resp);
          document_id=resp.data.document_id;
          $rootScope.$broadcast("SignHubEvent", "Get document details");
          return $http({
            method: 'GET',
            url:'https://api.signinghub.com/v2/documents/'+document_id+'/details',
            headers: {
              'Content-Type': 'application/json',
              'Accept':'application/json',
              'Authorization': 'Bearer '+access_token,
            },

          })
        }, function (resp) {
          console.log('Error status:');
          console.log(resp);
          $rootScope.$broadcast("SignHubError", resp);
        }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $rootScope.$broadcast("SignHubEvent", "Upload progress: "+progressPercentage+"%");
          // console.log('progress: ' + progressPercentage + '% ' + file.name);
        });
      },function(error) {
        $rootScope.$broadcast("SignHubError", error);
        console.log('error');
        return deferred.reject(error);
      }).then(function(responsa) {
        console.log(responsa);
        console.log(responsa.data.document_pages);

        total_document_pages=responsa.data.document_pages;
        $rootScope.$broadcast("SignHubEvent", "Apply template ("+total_document_pages+" pages)");
        return $http({
          method:'POST',
          url:'https://api.signinghub.com/v2/documents/'+document_id+'/workflow/template',
          headers: {
            'Content-Type': 'application/json',
            'Accept':'application/json',
            'Authorization': 'Bearer '+access_token,
          },
          data:{'template_name': "contract"+responsa.data.document_pages}

        })

      },function(errora) {
        $rootScope.$broadcast("SignHubError", errora);
      }).then(function(response) {
        console.log('response');
        console.log(response);
        $rootScope.$broadcast("SignHubEvent", "Apply signers to document ("+total_document_pages+" pages)");
        return $http({
          method:'PUT',
          //url:'https://api.signinghub.com/v2/documents/'+vm.docID+'/workflow/1/user',
          url:'https://api.signinghub.com/v2/documents/'+document_id+'/workflow/users',
          headers: {
            'Content-Type': 'application/json',
            'Accept':'application/json',
            'Authorization': 'Bearer '+access_token,
          },
          data:[ { order: 1, field_name: fieldnames[total_document_pages].field1, user_email: signer1.email, email_notification: false
          },
            { order: 2, field_name: fieldnames[total_document_pages].field2, user_email: signer2.email, email_notification: false
            }]

        }).then(function(response) {
          $rootScope.$broadcast("SignHubEvent", "Share contract to sign");
          console.log('response');
          console.log(response);
          $http({
            method:'POST',
            url:'https://api.signinghub.com/v2/documents/'+document_id+'/workflow',
            headers: {
              'Content-Type': 'application/json',
              'Accept':'application/json',
              'Authorization': 'Bearer '+access_token,
            },
            //data:[ { order: 1, field_name: 'placeholder', user_email: 'null@void.com', email_notification: false
            // }]
          })
        },function(error) {
          $rootScope.$broadcast("SignHubError", error);
          console.log('error');
          console.log(error);
        })


      },function(error) {
        console.log('error');
        console.log(error);
      }).then(function(response) {
        $rootScope.$broadcast("SignHubEvent", "Finalize contract");
        return Parse.Cloud.run('finalizeContract', {signer1:signer1,signer2:signer2,signID:document_id,title:contractTitle,userID:Parse.User.current().id});
        //vm.iframeSrc=$sce.trustAsResourceUrl('https://web.signinghub.com/Integration?access_token='+vm.tokenResponse+'&document_id='+vm.docID);
        console.log('response');
        console.log(response);
        //vm.tokenResponse=response.data.access_token
      },function(error) {
        $rootScope.$broadcast("SignHubError", error);
        console.log('error');
        console.log(error);
      }).then(function(response) {
        $rootScope.$broadcast("SignHubEvent", "complete");
        //vm.iframeSrc=$sce.trustAsResourceUrl('https://web.signinghub.com/Integration?access_token='+vm.tokenResponse+'&document_id='+vm.docID);
        console.log('response');
        console.log(response);
        //vm.tokenResponse=response.data.access_token
      },function(error) {
        $rootScope.$broadcast("SignHubError", error);
        console.log('error');
        console.log(error);
      });
    }

    /*
     Upload.upload({
     url: 'https://api.signinghub.com/v2/documents',
     method:'POST',
     headers : {
     'Content-Type': 'multipart/form-data; boundary= ----FormBoundaryE19zNvXGzXaLvS5C',
     'Accept':'application/json',
     'Authorization': 'Bearer '+vm.tokenResponse
     },
     data: {file:file}
     }).then(function (resp) {
     console.log('Success ');
     console.log(resp);
     vm.docID=resp.data.document_id;
     }, function (resp) {
     console.log('Error status:');
     console.log(resp);
     }, function (evt) {
     var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
     // console.log('progress: ' + progressPercentage + '% ' + file.name);
     });
    */


    function finalizeTimesheet() {

    }

  }
})();
