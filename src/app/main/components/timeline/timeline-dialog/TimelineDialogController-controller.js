'use strict';
/**
 * @ngdoc function
 * @name fuse.controller:timelinedialogcontrollerCtrl
 * @description
 * # timelinedialogcontrollerCtrl
 * Controller of the fuse
 */
angular.module('fuse')
  .controller('timelinedialogcontrollerCtrl', function ($scope, index, $mdDialog, Timeline) {
    var vm = this;

    if (index < 0) {
      vm.title = 'Create new role';
      vm.newRole = true;
    } else {
      vm.title = 'Edit Role';
      vm.event = Timeline.getEvent(index);
      vm.newRole = false;
    }

    vm.closeDialog = function () {
      $mdDialog.hide();
    }

    vm.deleteRole = function (index) {
      console.log('delete role');
      console.log(index);
    }

    vm.addNewRole = function () {
      console.log('add new role');
      //console.log(index);
    }

    vm.editRole = function (index) {
      console.log('edit role');
      console.log(index);
    }

    console.log('open timeline with index');
    console.log(index);

  });
