(function () {
  'use strict';

  angular
    .module('app.pages.profile')
    .controller('ProfileController', ProfileController);

  /** @ngInject */
  function ProfileController($scope, Candidate,$mdToast, AppUtil, $mdSticky, $rootScope, $mdDialog, Upload, $timeout, $linkedIn, GrabLnkdn, $window, uiGmapGoogleMapApi, $stateParams, fuseTheming) {
    var vm = this;

    fuseTheming.setActiveTheme('default');

    vm.profilePic=true;

    /* CONTAIN FORM DATA */
    vm.forms = {};
    vm.AttachForm = {};
    vm.maps={};
    vm.levels = ['Junion', 'Intermediate', 'Senior'];
    vm.prefRoles = [
      {label: 'Prefer permanent roles', value: 'perm'},
      {label: 'No preference', value: 'both'},
      {label: 'Prefer contract roles', value: 'contract'}
    ];



    Candidate.loadCandidate(Parse.User.current().id).then(function (succo) {

      vm.data=succo;
      vm.data.lastname='fcn';
      console.log('succo');
      console.log(succo);
      //vm.data=Candidate.serverData;
    }, function (erro) {
      console.log('erro');
      console.log(erro);
    })

    vm.changemap=function() {
      console.log('changemap');
   //   vm.maps.places.Autocomplete(document.getElementById('place'));
    }

    //////////
  }

})();
