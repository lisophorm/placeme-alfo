(function ()
{
    'use strict';

    angular
        .module('app.landing-candidate', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider,msNavigationServiceProvider)
    {
        $stateProvider.state('app.landing-candidate_page', {
            url  : '/landing-candidate',
            views: {
                'content@app': {
                    templateUrl: 'app/main/pages/landing-candidate/landing-candidate.html',
                    controller : 'LandingCandidateController as vm'
                }
            },
          resolve: {
            user: function (UserService) {
              console.log("*********** profile resolve");
              var value = UserService.init();
              return value;
            }
          }
        });

      msNavigationServiceProvider.saveItem('app.landing-candidate', {
        title : 'Landing',
        state : 'app.landing-candidate_page',
        weight: 1
      });
    }

})();
