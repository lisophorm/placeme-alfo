(function ()
{
    'use strict';

    angular
        .module('app.landing-candidate')
        .controller('LandingCandidateController', LandingCandidateController);

    /** @ngInject */
    function LandingCandidateController($state)
    {
        // Data
        var vm=this;
        // Methods
        vm.jumpToProfile=function(profiletab) {
          $state.go('app.pages_profile',{tabId:profiletab})
        }
        //////////
    }

})();
