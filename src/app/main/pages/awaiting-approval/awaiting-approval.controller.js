(function ()
{
    'use strict';

    angular
        .module('app.pages.awaiting-approval')
        .controller('AwaitingApprovalController', AwaitingApprovalController);

    /** @ngInject */
    function AwaitingApprovalController(fuseTheming)
    {
        var vm = this;

      fuseTheming.setActiveTheme('positive');

        // Data
        vm.endTime = 1472975790000;

        // Methods

        //////////
    }
})();
