(function ()
{
    'use strict';

    angular
        .module('app.pages.awaiting-approval', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider)
    {
        // State
        $stateProvider.state('app.pages_awaiting-approval', {
            url      : '/pages/awaiting-approval',
            views    : {
                'main@'                        : {
                    templateUrl: 'app/core/layouts/content-only.html',
                    controller : 'MainController as vm'
                },
                'content@app.pages_awaiting-approval': {
                    templateUrl: 'app/main/pages/awaiting-approval/awaiting-approval.html',
                    controller : 'AwaitingApprovalController as vm'
                }
            },
            bodyClass: 'awaiting-approval'
        });




    }

})();
