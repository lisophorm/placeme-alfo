(function () {
  'use strict';

  angular
    .module('app.pages.sign-candidate')
    .controller('SignCandidateController', SignCandidateController);

  /** @ngInject */
  function SignCandidateController($mdDialog, fuseTheming, Candidate, Timeline, AppUtil,$rootScope,$scope,$q,$document) {
    var vm = this;

    vm.timeline=[];

    fuseTheming.setActiveTheme('default');

    // Data
    vm.basicForm = {};
    vm.formWizard = {};

    Candidate.loadCandidate(Parse.User.current().id).then(function (succo) {

      vm.data = succo;
      vm.data.lastname = 'fcn';
      console.log('succo');
      console.log(succo);
      //vm.data=Candidate.serverData;
    }, function (erro) {
      console.log('erro');
      console.log(erro);
    });

    Timeline.loadTimeline(Parse.User.current().id,$scope).then(function (succo) {

      vm.timeline = succo;
      console.log('timeline');
      console.log(vm.timeline);
      //vm.data=Candidate.serverData;
    }, function (erro) {
      console.log('timeline erro');
      console.log(erro);
    });

    vm.loadNextPage=function() {
      console.log("*********** timeline loadnextpage");
      var deferred = $q.defer();
      deferred.resolve([]);
      return deferred.promise;
    }

    vm.saveCandidate = function () {
      $rootScope.loadingProgress = true;
      console.log("********* save candidate");
      Candidate.saveCandidate(vm.data).then(function (successo) {
        $rootScope.loadingProgress = false;
        console.log('candidato salvato');
      }, function (erroro) {
        $rootScope.loadingProgress = false;
        console.log('candidato erroro');
      })
    }

    vm.uploadProfile = function (croppedDataUrl) {
      $rootScope.loadingProgress = true;
      Candidate.uploadProfile(croppedDataUrl).then(function (reso) {
          $rootScope.loadingProgress = false;
          vm.data.profileImgURL = reso;
          vm.profilePic = null;
        },
        function (erro) {
          vm.profilePic = null;
          console.log('error uploading profile');
          console.log(erro);
          AppUtil.showErr(erro);
        })
    }

    vm.openTimeDialog = function(ev, index) {
      console.log('OPEN TIMELINE DIALOG');
      console.log(index);
      $mdDialog.show({
        controller: 'timelinedialogcontrollerCtrl',
        controllerAs: 'vm',
        templateUrl: 'app/main/components/timeline/timeline-dialog/timeline-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          index: index,
          event: ev
        }
      }).then(function(resso) {
        console.log("******** timeline dialog then");
      });;
    }

    vm.closeDialog=function() {

    }

    vm.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
    'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
    'WY').split(' ').map(function (state) {
      return {abbrev: state};
    });

    // Methods
    vm.sendForm = sendForm;

    //////////

    /**
     * Send form
     */
    function sendForm(ev) {
      // You can do an API call here to send the form to your server

      // Show the sent data.. you can delete this safely.
      $mdDialog.show({
        controller: function ($scope, $mdDialog, formWizardData) {
          $scope.formWizardData = formWizardData;
          $scope.closeDialog = function () {
            $mdDialog.hide();
          }
        },
        template: '<md-dialog>' +
        '  <md-dialog-content><h1>You have sent the form with the following data</h1><div><pre>{{formWizardData | json}}</pre></div></md-dialog-content>' +
        '  <md-dialog-actions>' +
        '    <md-button ng-click="closeDialog()" class="md-primary">' +
        '      Close' +
        '    </md-button>' +
        '  </md-dialog-actions>' +
        '</md-dialog>',
        parent: angular.element('body'),
        targetEvent: ev,
        locals: {
          formWizardData: vm.formWizard
        },
        clickOutsideToClose: true
      });

      // Clear the form data
      vm.formWizard = {};
    }
  }
})();
