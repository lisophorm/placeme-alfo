(function ()
{
    'use strict';

    angular
        .module('app.pages.sign-candidate', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider)
    {
        $stateProvider.state('app.pages_sign-candidate', {
            url      : '/sign-candidate',
            views    : {
              'main@'                        : {
                templateUrl: 'app/core/layouts/content-only.html',
                controller : 'MainController as vm'
              },
                'content@app.pages_sign-candidate': {
                    templateUrl: 'app/main/pages/sign-candidate/sign-candidate.html',
                    controller : 'SignCandidateController as vm'
                }
            },
            bodyClass: 'sign-candidate'
        });
    }

})();
