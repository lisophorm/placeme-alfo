(function ()
{
    'use strict';

    angular
        .module('app.pages.auth.register')
        .controller('RegisterController', RegisterController);

    /** @ngInject */
    function RegisterController($mdDialog,config,$http,AppUtil,$rootScope,UserService,$state,Flash,vcRecaptchaService,$window,$scope,fuseTheming)
    {
      var vm = this;

      // Data
      vm.alert = '';
        vm.password='';

      fuseTheming.setActiveTheme('positive');
      // Methods
      vm.showTerms = showTerms;
      vm.registerUser = registerUser;

       // vm.firstname=AppUtil.randomString(5);
       // vm.lastname=AppUtil.randomString(5);
       // vm.email=AppUtil.randomString(8)+"@"+AppUtil.randomString(5)+".com";
       // vm.password="bayer02";
       // vm.passwordConfirm="bayer02";


        vm.registerForm={};

        vm.pwdclass='';
        vm.pwdstrenghtlabel='NONE';
        vm.pwdstrenght=0;
        // recaptcha
        vm.response = null;
        vm.widgetId = null;
        vm.model = {
            key: '6LdA5xETAAAAAJDIoBfdhJk79TCpqJMlpTQw0Y5P'
        };
        vm.setResponse = function (response) {
            console.info('Response available');
            vm.response = response;
        };
        vm.setWidgetId = function (widgetId) {
            console.info('Created widget ID: %s', widgetId);
            vm.widgetId = widgetId;
        };
        vm.cbExpiration = function() {
            console.info('Captcha expired. Resetting response object');
            vm.response = null;
            $window.location.reload();
        };
        //

        $scope.$watch('vm.password', function(current, original) {
            console.log('password was '+ original);
            console.log('password is '+ current);
            vm.pwdstrenght=AppUtil.scorePassword(current);
            console.log('strenght: '+ vm.pwdstrenght);
            if(vm.pwdstrenght>=30) {
                vm.pwdstrenghtlabel='WEAK';
                vm.pwdclass='md-red-500-bg';
            }
            if(vm.pwdstrenght>60) {
                vm.pwdstrenghtlabel='MEDIUM';
                vm.pwdclass='md-yellow-500-bg';

            }
            if(vm.pwdstrenght>80) {
                vm.pwdstrenghtlabel='STRONG';
                vm.pwdclass='md-green-500-bg';
            }


        });

      function registerUser(ev) {


          var message='';
          Flash.dismiss();
          if(!vm.registerForm.$valid) {
              Flash.create('danger', 'Please correct all the required fields' );
              return false;
          }

          if(vm.pwdstrenght<50) {
              message = 'Password is too weak! Please find a password that is harder to guess (try adding add uppercase and special characters)';
              Flash.create('danger', message);
              return false;
          }
          if(!vm.response) {
              message = 'Please solve the captcha first!';
              Flash.create('danger', message);
              return false;
          }
          if(!vm.cb1) {
              message = 'You must first agree to our T&C';
              Flash.create('danger', message);
              return false;
          }
          if(vm.password!=vm.passwordConfirm) {
              message = 'Passwords do not match';
              Flash.create('danger', message);
              return false;
          }

        console.log("Registration!"+vm.firstname);
          $rootScope.loadingProgress = true;
        UserService.createUser({ username: vm.email,
            email:vm.email,
            password:vm.password,
          firstname:vm.firstname,
            lastname:vm.lastname,
            captchares:vm.response
        }).then(function (_data) {
            $rootScope.loadingProgress = false;
            $rootScope.user = _data;


          $state.go('app.pages_sign-candidate', {});

        }, function (_error) {
             $rootScope.loadingProgress = false;
          console.log("error creating user"+_error.code);
          console.log(_error);
            vcRecaptchaService.reload($scope.widgetId);
            //if(_error.code==666) {
                //vcRecaptchaService.reload($scope.widgetId);
            //}

            Flash.create('danger', _error.code+" "+_error.message);
          //AppUtil.showErr(_error.code+" "+_error.message);

          // alert("Error Creating User Account " + _error.debug)
        });
       /* var req = {
          method: 'POST',
          url: config.baseUrl+"users",
          headers: {
            //'Content-Type': 'text/plain',
            //'X-Parse-Application-Id': 'zAaLWS0PfGHZYlEE6ajMY1x29jUXgG4fjqBDCK91',
            //'X-Parse-REST-API-Key': 'VkBY2H6GeSEIqFBRybZlrq4zpAkYvID3OvRHLfFm',
            //'Access-Control-Allow-Origin': '*'
          },
          data: { username: vm.email,
          password:vm.password,
            firstname:vm.firstname,
            lastname:vm.lastname
          }
        }*/
    /*    $http(req).then(function successCallback(response) {

          console.log("succes");
          console.log(response);
        }, function errorCallback(response) {
          AppUtil.showErr(response.data.code+" "+response.data.description+" "+response.statusText+" - "+response.status);
          console.log("erroe");
          console.log(response);

        });*/
      }


        // Methods
      function showTerms(ev)
      {
        $mdDialog.show({
            controller         : function ($scope, $mdDialog)
            {
              $scope.closeTerms = function ()
              {
                $mdDialog.hide();
              };

              $scope.cancel = function ()
              {
                $mdDialog.cancel();
              };

              $scope.answer = function (answer)
              {
                $mdDialog.hide(answer);
              };
            },
            templateUrl        : 'termsandcond.tmpl.html',
            parent             : angular.element(document.body),
            targetEvent        : ev,
            clickOutsideToClose: true
          })
          .then(function (answer)
          {
            vm.alert = 'You said the information was "' + answer + '".';
          }, function ()
          {
            vm.alert = 'You cancelled the dialog.';
          });
      }
        //////////
    }
})();
