(function ()
{
    'use strict';

    angular
        .module('app.pages.auth.forgot-password')
        .controller('ForgotPasswordController', ForgotPasswordController);

    /** @ngInject */
    function ForgotPasswordController(Flash)
    {
        var vm = this;

        // Data

        vm.recover=function() {
            console.log('password recovery'+vm.form.email);

            Parse.User.requestPasswordReset(vm.form.email, {
                success: function() {
                    Flash.create('success', 'Please check your email for instructions');
                },
                error: function(error) {
                    Flash.create('danger', error.code+" "+error.message);
                }
            });

        };

        // Methods

        //////////
    }
})();