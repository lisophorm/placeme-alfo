(function () {
  'use strict';

  angular
    .module('app.pages.auth.login')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($scope, UserService, $location, $state, Flash, $rootScope, $linkedIn, $q, AppUtil, $window,fuseTheming) {
    var vm = this;
    vm.loggedIn=false;
    fuseTheming.setActiveTheme('positive');

    $rootScope.loadingProgress = false;
    $scope.$watch('vm.form.username', function handleChange(old) {
      console.log('gino tronico' + old);
      this.isDisabled = false;
    });
    if (($location.search()).changepass) {

      Flash.create('success', 'Your password has been changed!');
    }
    //if (($location.search()).code) {
    //  console.log("**************code");
    //  console.log($location.search().code);
    //  setTimeout(
    //    confirmLinkedin,2000      );
    //
    //}
    if (($location.search()).error_description) {

      Flash.create('danger', ($location.search()).error_description);
    }

    vm.isDisabled = true;
    vm.loginForm = {};

    vm.privateBrowsing = false;
    // Data

    // if private browsing is ON we cannot use this website
    var storageTestKey = 'sTest',
      storage = window.sessionStorage;

    try {
      storage.setItem(storageTestKey, 'test');
      storage.removeItem(storageTestKey);
    } catch (e) {
      if (e.code === DOMException.QUOTA_EXCEEDED_ERR && storage.length === 0) {
        vm.privateBrowsing = true;
        Flash.create('danger', 'We\'ve detected you are in PRIVATE BROWSING. The advanced funtionalites of this website require that PRIVATE BRWOSING must be turned off.');
      } else {
        throw e;
      }
    }


    if (($location.search()).username) {
      console.log('USERNAAAME');
      vm.form.username = (($location.search()).username);
    }
    //vm.linkedInClick = function () {
    //  window.location.href = 'https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=77c0sh4voemltg&redirect_uri=https://app.hybridrecruitment.com/pages/auth/login&state=987654321&scope=r_basicprofile,r_emailaddress';
    //
    //}
    vm.linkedInClick = function () {
      console.log('CLICK ON LINKEDIN');
      window.location.href = 'https://app.hybridrecruitment.com/authorize';
      /*     console.log(document.domain);
       document.domain = document.domain;
       console.log('CLICK ON LINKEDIN AUTHORIZE');
       $rootScope.loadingProgress = true;
       $linkedIn.authorize().then(function (result) {
       console.log('*********** LINKEDIN AUTHORIZED');
       AppUtil.showSuccess('LinkedIn Authorized!');
       console.log(result);
       return $linkedIn.profile('me', ['id', 'first-name', 'last-name', 'picture-url', 'email-address']);
       //                    return $linkedIn.profile('me',['id','first-name','last-name','headline','location','industry','summary','specialties','positions','picture-url','picture-urls','site-standard-profile-request','public-profile-url','email-address']);

       }).then(function (result) {
       AppUtil.showSuccess('Processing LinkedIn Identity...');
       console.log('*********** GOT LINKEDIN PROFILE');
       console.log(JSON.stringify(result.values[0]));
       return Parse.Cloud.run('loadLinkedInMember', result);
       }, function (error) {
       Flash.create('danger', error.toString());
       }).then(function (sessionToken) {
       AppUtil.showSuccess('Migrating Session Token');
       console.log('FROM DA CLOUD');
       console.log('session token:');
       console.log(sessionToken);
       return Parse.User.become(sessionToken);
       }, function (error) {
       $window.trackJs.track(error);
       Flash.create('danger', JSON.stringify(error));
       console.log('*********** ERROR DA CLOUD');
       console.log(error);
       console.log('session token:');
       //console.log(sessionToken);
       return $q.reject();
       }).then(function (user) {
       return Parse.Cloud.run('isAdmin');
       },
       function (error) {
       $window.trackJs.track(error);
       Flash.create('danger', JSON.stringify(error));
       console.log('NOT BECOME USER!!!');

       }).then(function (reso) {
       $rootScope.loadingProgress = false;
       if (reso.isAdmin) {
       $rootScope.linkedInAdmin = true;
       AppUtil.showSuccess('Obtaining SuperAdmin powers!');
       console.log("*********** ADMIN");
       $state.go('app.dashboards_project');
       } else {
       console.log("*********** CANDIDATE");
       $state.go('app.pages_profile');
       }
       AppUtil.storeUser('isAdmin', reso.isAdmin);
       $rootScope.isAdmin = reso.isAdmin;

       }, function (error) {
       $window.trackJs.track(error);
       $rootScope.loadingProgress = false;
       $rootScope.isAdmin = false;
       console.log(error);
       }
       )
       */
    };

    // Methods
    vm.login = function () {

      if (!vm.loginForm.$valid) {
        Flash.create('danger', 'Please correct the fields highlited in red');
        return false;
      }
      $rootScope.loadingProgress = true;
      vm.errorMsg = '';
      UserService.login(vm.username, vm.password)
        .then(function (_response) {
          //alert('login success ' + _response.attributes.username);
          vm.loggedIn=true;
          console.log('logged in');
          console.log(_response);
          AppUtil.storeUser('isAdmin', _response.isAdmin);
          AppUtil.storeUser('superAdmin', _response.superAdmin);
          AppUtil.storeUser('roleType', _response.roleType);
          $rootScope.isAdmin = _response.isAdmin;
          $rootScope.superAdmin = _response.superAdmin;
          $rootScope.roleType=_response.roleType;

          return Parse.User.become(_response.session_token);

        }, function (_error) {
          vm.loggedIn=false;
          console.log('error login');
          console.log($rootScope.loadingProgress);
          console.log(_error);
          $rootScope.loadingProgress = false;
          Flash.create('danger', _error.code + ' ' + _error.message);
        })
        .then(function (succo) {
          if(vm.loggedIn) {

            console.log('I AM USER');
            console.log(succo.get(''))
            console.log("after logged in");
            $rootScope.loadingProgress = false;

            if (typeof $rootScope.returnState !== 'undefined') {
              console.log('RETURN STATE');
              console.log($rootScope.returnState);
              $state.go($rootScope.returnState.name, $rootScope.returnToStateParams);
            } else if ($rootScope.isAdmin || $rootScope.superAdmin) {
              console.log("*********** ADMIN");
              $state.go('app.landing-candidate_page');
            } else {
              console.log("*********** CANDIDATE");
              $state.go('app.landing-candidate_page');
            }
          }
        }, function (erri) {
          if(typeof erri !=='undefined') {
            AppUtil.showErr(erri);
          }

        });
    };

    vm.facebookLogin = function () {
      $rootScope.loadingProgress = true;
      console.log('FB login');
      Parse.FacebookUtils.logIn('email', {
        success: function (user) {
          if (!user.existed()) {
            console.log('User signed up and logged in through Facebook!');
            //$state.go('app.pages_profile');
            FB.api('me', 'get', {fields: 'id,name,email,cover,first_name,last_name'}, function (response) { // jshint ignore:line
              console.log(Parse.User.current().get('first_name'));
              console.log('response');
              console.log(response);
              setFromFB(response);

            });
          } else {
            console.log('User logged in through Facebook!');
            if (typeof $rootScope.returnState !== 'undefined') {
              console.log('RETURN STATE');
              console.log($rootScope.returnState);
              $state.go($rootScope.returnState.name, $rootScope.returnToStateParams);
            } else {
              $state.go('app.pages_profile');
            }

            //
          }
        },
        error: function (user, error) {
          $rootScope.loadingProgress = false;
          console.log('FB login error');
          console.log(error);
          $window.trackJs.track(error);
          Flash.create('danger', error.code + ' ' + error.message);
          console.log('Facebook login error: /r/nUser cancelled the Facebook login or did not fully authorize.');
        }
      });
    };
    function setFromFB(fbRes) {
      console.log('seting user' + Parse.User.current().id);
      console.log(fbRes.first_name);
      Parse.User.current().set('firstname', fbRes.first_name);
      //user.set('email',fbRes.email);
      Parse.User.current().set('isFB', true);
      Parse.User.current().set('lastname', fbRes.last_name);
      Parse.User.current().set('fbcover', fbRes.cover);
      FB.api('/me/picture?width=400&height=400', function (response) {
        console.log('********* got the photo');
        console.log(response.data.url);
        Parse.User.current().set('isFB', true);
        Parse.User.current().set('photourl', response.data.url);
        Parse.User.current().save().then(function () {
          $rootScope.roleType='Candidate';
          $state.go('app.pages_profile');
        });
      });

      console.log('user query success');
      console.log(user.get('firstname'));
    }

  }
})();
