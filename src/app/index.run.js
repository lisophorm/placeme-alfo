(function () {
  'use strict';

  angular
    .module('fuse')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, $timeout, config, $location, UserService, $state, Analytics, $cookies, $http) {


    $http.defaults.useXDomain = true;
    var service = UserService.init();

    $rootScope.$watch('$root.loadingProgress', function () {
      console.log("************** $root.loadingProgress");
      //console.log($root.daUser);
      if ($rootScope.loadingProgress) {
        console.log("************** ROOT LOAD YES");
      } else {
        console.log("************** ROOT LOAD NO");
      }
      // $scope.userPLevel = $rootScope.uPLevel;
    });

    if (typeof $rootScope.isAdmin === 'undefined') {
      console.log('******** ADMIN IS UNDEFINED');
      try {
        if (typeof $cookies.getObject('userStuff').isAdmin !== 'undefined') {
          console.log('******** ADMIN COOKIE IS DEFINED');
          $rootScope.isAdmin = $cookies.getObject('userStuff').isAdmin;
          $rootScope.superAdmin = $cookies.getObject('userStuff').superAdmin;
          $rootScope.roleType = $cookies.getObject('userStuff').roleType;
          if ($rootScope.isAdmin) {
            console.log('******** ADMIN FROM COOKIE ');
          } else {
            console.log('******** USER FROM COOKIE ');
          }
          if ($rootScope.superAdmin) {
            console.log('******** SUPER ADMIN FROM COOKIE ');
          } else {
            console.log('******** USER FROM COOKIE ');
          }
        }
      } catch (e) {
        console.log('******** ADMIN COOKIE IS UN-DEFINED');
        $rootScope.isAdmin = false;
        $rootScope.superAdmin = false;
      }
    }


    console.log('run');
    console.log(config);
    $rootScope.$on('$stateChangeError',
      function (event, toState, toParams, fromState, fromParams, error) {

        console.log('******** STATECHANGEERROR ');
        console.log(toState.name);
        console.log('$stateChangeError ' + error && (error.debug || error.message || error));
        console.log('******** TOSTATE NAME ' + toState.name);
        if (toState.name.indexOf('auth') > -1) {
          console.log('************ inside auth');
          return;
        }
        console.log('************ RUN');
        console.log(error);
        // if the error is 'noUser' the go to login state
        if (error && error.error === 'noUser') {
          console.log('NO USER RUN');
          $rootScope.returnState = angular.copy(toState);
          $rootScope.returnToStateParams = angular.copy(toParams);
          console.log($rootScope.returnState);
          console.log($rootScope.returnToStateParams);
          //$rootScope.returnToState = toState.url;
          //$rootScope.returnToStateParams = toParams.Id;
          event.preventDefault();

          $state.go('app.pages_auth_login', {});

        } else if (error && error.error === 'regoIncomplete') {
          console.log("********** RUN OTHER ERROR regoIncomplete");
          event.preventDefault();

          $state.go('app.pages_sign-candidate', {});
        } else if (error && error.error === 'regoAwaiting') {
          console.log("********** RUN OTHER ERROR regoAwaiting");
          event.preventDefault();

          $state.go('app.pages_awaiting-approval', {});
        }
      });

    var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function (evt, toState) {
      //var service = UserService.init();
      console.log('state change start');
      console.log(toState);

      if(toState.url.indexOf('logout')>-1) {
        console.log("********* cacca");
        UserService.logout().then(function(xx) {
          location.href='/pages/auth/login';
        })
      }

      console.log(evt);
      console.log('user:');
      //console.log(service);
      $rootScope.loadingProgress = true;


    });

    var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function () {
      $timeout(function () {
        $rootScope.loadingProgress = false;
      });
    });

    // Store state in the root scope for easy access
    $rootScope.state = $state;

    // Cleanup
    $rootScope.$on('$destroy', function () {
      stateChangeStartEvent();
      stateChangeSuccessEvent();
    })

  }

})
();
