var sendgrid = require("sendgrid");
sendgrid.initialize("hybridrecruitment", "k0st0golov");

Parse.Cloud.define("mySendGridFunction", function(request, response) {
  sendgrid.sendEmail({
    to: "community@parse.com",
    from: "sendgrid@parse.com",
    subject: "Hello from Cloud Code!",
    text: "Using Parse and SendGrid is great!"
  }, {
    success: function(httpResponse) { response.success("Email sent!"); },
    error: function(httpResponse) { response.error("Uh oh, something went wrong"); }
  });
});


sendgrid.sendEmail({
  to: ["lisophorm@gmail.com"],
  from: "SendGrid@CloudCode.com (mailto:SendGrid@CloudCode.com)",
  subject: "Hello from Cloud Code!",
  text: "Using Parse and SendGrid is great!",
  replyto: "reply@example.com (mailto:reply@example.com)"
}).then(function(httpResponse) {
  console.log(httpResponse);
  response.success("Email sent!");
},function(httpResponse) {
  console.error(httpResponse);
  response.error("Uh oh, something went wrong");
});
