var express = require('express');
var querystring = require('querystring');
var _ = require('underscore');
var Buffer = require('buffer').Buffer;
var app = express();
var parseExpressCookieSession = require('parse-express-cookie-session');
app.use(express.cookieParser('monkeylove007'));
app.use(parseExpressCookieSession(
  {
    cookie: {maxAge: 3600000}
  }
));
app.set('views', 'cloud/views');  // Specify the folder to find templates
app.set('view engine', 'ejs');    // Set the template engine
app.use(express.bodyParser());    // Middleware for reading request
app.use(app.router);
var parseExpressHttpsRedirect = require('parse-express-https-redirect');
app.use(parseExpressHttpsRedirect());

var moment = require('moment');
var shortDateFormat = "DD/MM/YYYY"; // this is just an example of storing a date format once so you can change it in one place and have it propagate
app.locals.moment = moment; // this makes moment available as a variable in every EJS page
app.locals.shortDateFormat = shortDateFormat;
//var cookieParser = require('cookie-parser'); // the session is stored in a cookie, so we use this to p

//push state interceptors
var pushUrlPrefixes = [""];
var index = null;

var sendgrid = require("sendgrid");
sendgrid.initialize("hybridrecruitment", "k0st0golov");

app.get('/testo', function (req, res) {
  Parse.Cloud.useMasterKey();
  console.log("searchin for ");
  console.log(req.query.userid);
  var response = "";
  var queryCode = new Parse.Query('UserInfo');
  queryCode.equalTo('objectId', req.query.userid);
  queryCode.first().then(function (obj) {
    if (typeof obj !== 'undefined') {
      console.log("USER ID:");
      console.log(obj);
      res.send(obj);
    } else {
      console.log("USER IDnonono");

    }

  }, function (error) {
    console.log("ERRORE QUI");
    console.log(error);

  })
  console.log(response);

});

Parse.Cloud.define('contactCandidate', function (req, response) {
  console.log('candidate contacted');
  console.log(req.params.jobID);
  console.log(req.params.candID);
  jobData = {};
  candData = {};
  var searchJob = new Parse.Query('JobBag');
  var searchPpl = new Parse.Query('JobPeople');
  searchJob.equalTo('objectId', req.params.jobID);
  searchPpl.equalTo('objectId', req.params.candID);
  searchJob.first().then(function (response1) {
    if (typeof response1 !== 'undefined') {
      jobData = response1;
      return searchPpl.first();
    } else {
      response.error('Job Bag not found. Contact sysadmin');
    }

  }, function (error) {
    response.error(error);
  }).then(function (response2) {
    if (typeof response2 !== 'undefined') {
      candData = response2;
      //response.success('email sent');
      return emailJobOffer(jobData, candData);
    } else {
      response.error('Person not found. Contact sysadmin');
    }

  }, function (error) {
    response.error(error);
  }).then(function (success) {
    console.log("********* EMAIL SEND PROMISE");
    candData.set('contactedDate', new Date());
    return candData.save();
  }, function (error) {
    console.log("********* EMAIL SEND PROMISE REJECT");
    console.log(error);
    response.error(error);
  }).then(function (success) {
    console.log("********* REQUEST STATUS SUCCESS");
    response.success('candidate updated');
  }, function (error) {
    console.log("********* EMAIL SEND PROMISE REJECT");
    console.log(error);
    response.error(error);
  });
});

function emailJobOffer(jobOBJ, candOBJ) {
  console.log('job email function');
  var promise = new Parse.Promise();
  var candidate = {
    id: candOBJ.id,
    name: candOBJ.get('firstname') + candOBJ.get('lastname'),
    email: candOBJ.get('email')
  };
  var jobOffer = {
    id: jobOBJ.id,
    companyName: jobOBJ.get('companyInfo').name,
    title: jobOBJ.get('title'),
    internalCode: jobOBJ.get('internalCode'),
    briefText: jobOBJ.get('briefText'),
    startDate: jobOBJ.get('startDate'),
    dueDate: jobOBJ.get('dueDate'),
    title: jobOBJ.get('title')
  };
  if (jobOBJ.get('hasBriefAttach')) {
    jobOffer.hasBriefAttach = true;
    jobOffer.jobAttach = jobOBJ.get('attach');
    //console.log('jobAttach**********************');
    //console.log(jobAttach);
    //console.log(jobOffer.jobAttach.type());
    jobOffer.jobFileName = jobOBJ.get('fileName');
  }

  var sendGrid = require("sendgrid");
  sendGrid.initialize("hybridrecruitment", "k0st0golov");
  var email = sendGrid.Email({to: ['lisophorm@gmail.com']});
  email.setFrom('admin@hybridrecruitment.com');
  // email.addTo('lisophorm@gmail.com');
  email.setHTML('<b></b>');
  email.addFilter('templates', 'template_id', '9d88fc18-11d4-4d1f-b4dd-8eef437a62d7');
  email.addUniqueArg('candidateID', candidate.id);
  //email.header.sub=subs;
  email.setSections({
    ":event_details": [
      ":event1",
      ":event2",
      ":event1"
    ]
  });
  email.addSubstitution("-companyName-", jobOffer.companyName);
  email.addSubstitution("-title-", jobOffer.title);
  email.addSubstitution("-internalCode-", jobOffer.internalCode);
  email.addSubstitution("-briefText-", jobOffer.briefText);
  email.addSubstitution("-startDate-", formattedDate(jobOffer.startDate));
  email.addSubstitution("-dueDate-", formattedDate(jobOffer.dueDate));
  email.addSubstitution("-candidateID-", candidate.id);
  email.setSubject('Job proposal:' + jobOffer.title);
  console.log('email section');
  console.log(email.header.section);

  if (jobOffer.hasBriefAttach) {
    Parse.Cloud.httpRequest({url: jobOffer.jobAttach.url()}).then(function (e) {
      console.log('attached the attachment****************');
      //console.log(jobOffer.jobAttach.url());
      //console.log(jobOffer.jobFileName);
      //email.body['files[' + jobOffer.jobFileName + ']'] = e.buffer.toString('base64');
      //email.addFileFromBuffer(jobOffer.jobFileName, e.buffer);
      //console.log(e.buffer.toString('base64'));
      return sendGrid.send(email);
    }, function (error) {
      console.log('error with the attachment***************');
      console.log(error);
    });
  } else {
    return sendGrid.send(email);
  }


}

function formattedDate(date) {
  var d = new Date(date || Date.now()),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [day, month, year].join('/');
}


app.get('/arro', function (req, res) {
  Parse.Cloud.useMasterKey();
  console.log("searchin for ");
  var response = '{  "keys": ["userid","user"] ,"values": [';
  var JobBag = Parse.Object.extend("UserInfo");
  var queryCode = new Parse.Query(JobBag);
  // queryCode.equalTo('objectId',req.param("obj"))
  queryCode.find().then(function (objects) {
    console.log("SONO QUI");
    console.log(objects);
    for (var i = 0; i < 51; i++) {
      var item = objects[i];
      response += '["' + item.id.toString() + '","' + item.get('firstname') + '"],\r\n';
    }
    i++;
    response += '["' + item.id.toString() + '","' + item.get('firstname') + '"]\r\n';
    response += '  ] }';
    res.send(response);
  }, function (error) {
    console.log("ERRORE QUI");
    console.log(error);

  })
  console.log(response);
  //
  //var queryCode = new Parse.Query('JobBag');4h
//  queryCode.first().then(function(obj) {
  //
  // });
});


var querystring = require('querystring');

app.get('/timesheet/:jobID', function (req, res) {
  console.log('timesheet');
  var timeSheetId = req.params.jobID.toString();
  console.log(timeSheetId);
  jobData = {};
  candData = {};
  Parse.Cloud.useMasterKey();
  var TimeSheet = Parse.Object.extend("SigninDocuments");
  var timeQuery = new Parse.Query(TimeSheet);
  timeQuery.equalTo('objectId', timeSheetId);
  timeQuery.first().then(function (response1) {
    if (typeof response1 !== 'undefined') {
      jobData = response1;
      res.render('timesheet', {
        message: 'fangu',
        weekModel: jobData.get('weekModel'),
        id: jobData.id,
        timeModel: jobData.get('timeModel'),
        totalTime: jobData.get('totalTime'),
        signerName: jobData.get('signerName'),
        signerEmail: jobData.get('signerEmail'),
        signerRole: jobData.get('signerRole'),
        projectTitle: jobData.get('projectTitle'),
        authorName: jobData.get('authorName'),
        authorEmail: jobData.get('authorEmail')


      });
    } else {
      res.send('Timesheet not found - Contact sysadmin ' + req.param('jobID'));
    }

  }, function (error) {
    res.send(error);
  });

});


app.get('/joboffer/:jobID', function (req, res) {
  console.log('candidate contacted');
  console.log(req.params.jobID);
  console.log(req.params.candID);
  jobData = {};
  candData = {};


  var searchJob = new Parse.Query('JobBag');
  var searchPpl = new Parse.Query('JobPeople');
  searchJob.equalTo('objectId', req.param('jobID'));
  searchJob.first().then(function (response1) {
    if (typeof response1 !== 'undefined') {
      jobData = response1;
      res.render('jobposting', {
        message: 'fangu',
        internalCode: jobData.get('internalCode'),
        id: jobData.id,
        title: jobData.get('title'),
        description: jobData.get('jobDescription')


      });
    } else {
      res.send('Job Bag not found. Contact sysadmin');
    }

  }, function (error) {
    res.send(error);
  });

});


app.get('/getBrief/:fileID', function (req, res) {
  Parse.Cloud.useMasterKey();
  var fileInfo = {}
  var EmailReports = Parse.Object.extend("JobBag");
  var searchFile = new Parse.Query(EmailReports);
  searchFile.equalTo('objectId', req.param('fileID'));
  searchFile.first().then(function (obj) {
    if (typeof obj !== 'undefined') {
      if (obj.get('hasBriefAttach')) {
        spitFile(obj, res);
      } else {

      }

    } else {
      res.send('File not found. Contact sysadmin ');
    }
  }, function (error) {
    res.send(error);
  });

});

app.get('/getFile/:fileID', function (req, res) {
  Parse.Cloud.useMasterKey();
  var fileInfo = {}
  var EmailReports = Parse.Object.extend("WorkAttachments");
  var searchFile = new Parse.Query(EmailReports);
  searchFile.equalTo('objectId', req.param('fileID'));
  searchFile.first().then(function (obj) {
    if (typeof obj !== 'undefined') {
      spitFile(obj, res);
    } else {
      res.send('File not found. Contact sysadmin ');
    }
  }, function (error) {
    res.send(error);
  });

});


function spitFile(obj, res) {
  Parse.Cloud.useMasterKey();
  var fileInfo = {};
  console.log('found it');
  console.log(obj.get('attach').url());
  fileInfo.name = obj.get('attach').name();
  console.log(fileInfo.name);
  Parse.Cloud.httpRequest({
    method: 'GET',
    url: obj.get('attach').url()
  }).then(function (response) {
    res.setHeader('Content-disposition', 'attachment; filename=' + fileInfo.name);
    res.end(response.buffer.toString('binary'), 'binary');
    //res.send(response.buffer);
  }, function (error) {
    res.send(error);
  });

}

app.post('/BitBucketHook', function (req, res) {
  console.log("********* bitbucket");
  Parse.Cloud.useMasterKey();
  var reports = [];
  var EmailReports = Parse.Object.extend("Reports");
  console.log(req.body);
  var emailReport = new EmailReports();
  emailReport.set('test', JSON.stringify(req.body));
  emailReport.save();
  /*  for (var i = 0; i < req.body.length; i++) {
   var current=req.body[i];
   var emailReport = new EmailReports();
   emailReport.set(current);
   if (typeof current.candidateID !== 'undefined') {
   emailReport.set('cazzarola', 5);
   var searchJob = new Parse.Query('JobPeople');
   searchJob.equalTo('objectId', req.body[i].candidateID);
   searchJob.first().then(function (response1) {
   if (typeof response1 !== 'undefined') {
   emailReport.set('nullo', 3);
   response1.set('lastAction',current.event);
   response1.set('lastActionTime',new Date());
   response1.save();
   emailReport.set('nullo', 3);
   } else {

   emailReport.set('nullo', 5);
   }
   });
   }
   else {
   emailReport.set('nullo', JSON.stringify(current));
   }
   reports.push(emailReport);
   }
   Parse.Object.saveAll(reports).then(function (success) {
   res.send('req saved');
   }, function (error) {
   res.send(error);
   });*/

//res.send(req.body);
  //res.send(JSON.stringify(req.code));

  //var queryCode = new Parse.Query('JobBag');
//  queryCode.first().then(function(obj) {
  //  res.send(JSON.stringify(obj));
  // });
});
var signPassword = 'k0st0golov';
var signApikey = '939B2CFD3EC41BA954FE4CEA8C169D7DFC7D06959BD1D574E14641BB6785DD07';
var signcCient_id = 'HybridRecruitment';
var signTemplatename = 'template02';
var signMasterUser = 'lisophorm@gmail.com';

app.get('/signTheContract/:id', function (req, res, next) {
  console.log(Parse.User.current());
  Parse.Cloud.run('isAdmin',{currentUser:req.user.id}).then(function (ammo) {
    res.send(ammo);
  }, function (erro) {
    res.send(erro);
  });
  //console.log('Fetched question: '+req.params.id);
  //res.redirect('http://www.google.com');

});

app.get('/createUserFromBackend/:email', function (req, res) {
  var newUserID='';
  Parse.Cloud.useMasterKey();
  //res.error('ciao');
  //res.render('main');
  var UserInfo = Parse.Object.extend("UserInfo");
  var User = Parse.Object.extend("_User");
  var user=new User();
  var userInfo=new UserInfo();

  user.set('username', req.params.email);
  user.set('password', 'changeme');
  user.set('email', req.params.email);
  user.set('firstname', 'changeme');
  user.set('lastname', 'changeme');
  //user.set('captchares', req.params.email);
  user.set('backend', true);
  user.save().then(function(succo){
    console.log('NEW USER');
    console.log(succo.id);

    userInfo.set('newFromBackend',true);
    userInfo.set('userID',succo);
    userInfo.set('email', req.params.email);
    userInfo.set('firstname', 'changeme');
    userInfo.set('lastname', 'changeme');
    return userInfo.save();
  },function(erro) {
    res.send(JSON.stringify(erro));
  }).then(function(succo2){
   // res.send('all done');
    newUserID=succo2.id;
    res.redirect('/private-profile/'+newUserID);

  },function(erro2) {
    res.send(JSON.stringify(erro2));
  });
  console.log(Parse.User.current());

  //Parse.Cloud.run('isAdmin',{currentUser:req.user.id}).then(function (ammo) {
    //res.send(ammo);
 // }, function (erro) {
 //   res.send(erro);
//  });
  //console.log('Fetched question: '+req.params.id);
  //

});


app.get('/signingHub', function (req, res) {
  var docu_status = '';
  Parse.Cloud.useMasterKey();
  var docObject = {};
  var access_token = '';
  var SIgnTimeSheet = Parse.Object.extend("SigninDocuments");
  var queryCode = new Parse.Query(SIgnTimeSheet);
  queryCode.equalTo('signID', req.query.document_id);
  queryCode.first().then(function (result) {
    docObject = result;
    return Parse.Cloud.httpRequest({
      url: 'https://api.signinghub.com/authenticate',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      },
      body: 'grant_type=password&client_id=' + signcCient_id
      + '&client_secret=' + signApikey + '&username='
      + signMasterUser + '&password='
      + signPassword
    });

  }, function (error) {
    res.send('error writing document info: ' + JSON.stringify(error));
  }).then(function (httpResponse) {
      access_token = httpResponse.data.access_token;
      return Parse.Cloud.httpRequest({
        url: 'https://api.signinghub.com/v2/documents/' + req.query.document_id + '/log',
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + access_token

        }
      });

    },
    function (httpResponse) {
      console.log('Failed with: ' + httpResponse.status);
      console.log(httpResponse);
      res.send(httpResponse.text);
    }).then(function (cocco) {
    var actions = JSON.parse(cocco.text).actions;
    docu_status = JSON.parse(cocco.text).document_status;
    console.log('actions');
    console.log(actions);
    var no_res = true;
    for (var i = 0; i < actions.length; i++) {
      var action = actions[i];
      console.log("Action " + i + " " + action.action_type + " +" + action.information.value);
      if (action.action_type == "DECLINED_DOCUMENT") {
        no_res = false;
        docu_action = 'declined';
        docObject.set('status', 'declined');
        docObject.set('reason', action.information.value);
        return docObject.save();
        break;
      }
      if (action.action_type == "SIGNED") {
        no_res = false;
        docu_action = 'signed';
        docObject.set('status', 'signed');
        docObject.set('reason', docu_status);
        return docObject.save();
        break;
      }
    }
    console.log('actions length');
    console.log(actions.length);
    if (no_res) {
      no_res = false;

      docObject.set('status', action.action_type);
      docObject.set('reason', action.information.value);
      return docObject.save();

    }

  }, function (erro) {
    res.send(JSON.stringify(erro));
  }).then(function (oggetto) {
    res.render("thankyou");
  }, function (error1) {
    res.send("Error while saving results - " + JSON.stringify(error1));

  });

});

app.post('/sendGridHook', function (req, res) {
  Parse.Cloud.useMasterKey();
  console.log('sendgrid hook');
  var reports = [];
  var EmailReports = Parse.Object.extend("EmailReports");
  for (var i = 0; i < req.body.length; i++) {
    var current = req.body[i];
    var emailReport = new EmailReports();
    emailReport.set(current);
    if (typeof current.candidateID !== 'undefined') {
      var searchJob = new Parse.Query('JobPeople');
      searchJob.equalTo('objectId', req.body[i].candidateID);
      searchJob.first().then(function (response1) {
        if (typeof response1 !== 'undefined') {
          emailReport.set('nullo', '3');
          response1.set('lastAction', current.event);
          response1.set('lastActionTime', new Date());
          response1.save();
          emailReport.set('nullo', '3');
        } else {

          emailReport.set('nullo', '5');
        }
      });
    }
    else {
      emailReport.set('nullo', JSON.stringify(current));
    }
    reports.push(emailReport);
  }
  Parse.Object.saveAll(reports).then(function (success) {
    //console.log('sendgrid event saved');
    res.send('req saved');
  }, function (error) {
    //console.log('sendgrid error');
    //console.log(error);
    res.send(error);
  });

//res.send(req.body);
  //res.send(JSON.stringify(req.code));

  //var queryCode = new Parse.Query('JobBag');
//  queryCode.first().then(function(obj) {
  //  res.send(JSON.stringify(obj));
  // });
});

function getIndex(cb) {
  return function (req, res) {
    Parse.Cloud.httpRequest({
      url: 'http://app.hybridrecruitment.com',
      success: function (httpResponse) {
        index = httpResponse.text;
        cb(req, res);
      },
      error: function (httpResponse) {
        console.log('error');
        console.log(httpResponse);
        res.send("We're very busy at the moment, try again soon:" + httpResponse.text);

      }
    });
  }
}

/*********** LINKEDIN
 *
 *
 */
var restrictedAcl = new Parse.ACL();
restrictedAcl.setPublicReadAccess(false);
restrictedAcl.setPublicWriteAccess(false);

var linkedinClientId = '77c0sh4voemltg';
var linkedinClientSecret = 'TIKtuiRZ2xs6BKkw';

var linkedinRedirectEndpoint = 'https://www.linkedin.com/uas/oauth2/authorization?';
var linkedinValidateEndpoint = 'https://www.linkedin.com/uas/oauth2/accessToken';
var linkedinUserEndpoint = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,picture-url,email-address)';


var TokenRequest = Parse.Object.extend("TokenRequest");
var TokenStorage = Parse.Object.extend("TokenStorage");


var restrictedAcl = new Parse.ACL();
restrictedAcl.setPublicReadAccess(false);
restrictedAcl.setPublicWriteAccess(false);

app.get('/authorize', function (req, res) {
  console.log('within authorize function');
  var redirectUri = "https://app.hybridrecruitment.com/oauthCallback";
  var responseType = "code";
  var tokenRequest = new TokenRequest();
  // Secure the object against public access.
  tokenRequest.setACL(restrictedAcl);
  /**
   * Save this request in a Parse Object for validation when linkedin responds
   * Use the master key because this class is protected
   */
  tokenRequest.save(null, {useMasterKey: true}).then(function (obj) {
    /**
     * Redirect the browser to linkedin for authorization.
     * This uses the objectId of the new TokenRequest as the 'state'
     *   variable in the linkedin redirect.
     */
    res.redirect(
      linkedinRedirectEndpoint + querystring.stringify({
        client_id: linkedinClientId,
        state: obj.id,
        response_type: responseType,
        redirect_uri: redirectUri
      })
    );
  }, function (error) {
    // If there's an error storing the request, render the error page.
    res.render('error', {errorMessage: 'Failed to save auth request.'});
  });

});
app.get('/oauthCallback', function (req, res) {
  var data = req.query;
  var token;
  /**
   * Validate that code and state have been passed in as query parameters.
   * Render an error page if this is invalid.
   */
  if (!(data && data.code && data.state)) {
    res.render('error', {errorMessage: 'Invalid auth response received.'});
    return;
  }
  var query = new Parse.Query(TokenRequest);
  /**
   * Check if the provided state object exists as a TokenRequest
   * Use the master key as operations on TokenRequest are protected
   */
  Parse.Cloud.useMasterKey();
  Parse.Promise.as().then(function () {
    return query.get(data.state);
  }).then(function (obj) {
    // Destroy the TokenRequest before continuing.
    return obj.destroy();
  }).then(function () {
    // Validate & Exchange the code parameter for an access token from linkedin
    return getlinkedinAccessToken(data.code);
  }).then(function (access) {
    /**
     * Process the response from linkedin, return either the getlinkedinUserDetails
     *   promise, or reject the promise.
     */
    var linkedinData = access.data;
    if (linkedinData && linkedinData.access_token) {
      token = linkedinData.access_token;
      return getlinkedinUserDetails(token);
    } else {
      return Parse.Promise.error("Invalid access request.");
    }
  }).then(function (userDataResponse) {
    /**
     * Process the users linkedin details, return either the upsertlinkedinUser
     *   promise, or reject the promise.
     */
    /*var userDataXml = userDataResponse.text;
     $userDataXml = $( userDataXml ),
     var userData = $userDataXml.find('id');*/
    var userData = userDataResponse.data;
    console.log('linkedin user data:');
    console.log(userData);
    if (userData && userData.id) {
      return upsertlinkedinUser(token, userData);
    } else {
      return Parse.Promise.error("Unable to parse Linkedin data");
    }
  }).then(function (user) {
    /**
     * Render a page which sets the current user on the client-side and then
     *   redirects to /main
     */
    res.render('store_auth', {sessionToken: user.getSessionToken()});
  }, function (error) {
    /**
     * If the error is an object error (e.g. from a Parse function) convert it
     *   to a string for display to the user.
     */
    if (error && error.code && error.error) {
      error = error.code + ' ' + error.error;
    }
    res.render('error', {errorMessage: JSON.stringify(error)});
  });

});

/* login functions */
Parse.Cloud.define('loginCloud', function (request, response) {
  console.log('login from server'+request.body.pwd);
  var session_token='';
  var proceed=true;
  console.log(request);
    Parse.User.logIn(request.params.email, request.params.pwd).then(
      function(user) {
        console.log('login success');
        console.log(user.id);
        console.log('*******');
        console.log(user.getSessionToken());
        session_token=user.getSessionToken();
        console.log(user);
        return Parse.Cloud.run('isAdmin',{currentUser:user.id});
      },
      function(error) {
        console.log('login error');
        console.log(error);
        proceed=false;
       response.error(error);
      }
    ).then(function(resulto) {
      console.log('success isadmin');
      console.log(resulto);
      if(proceed) {
        proceed=false;
        response.success({'session_token':session_token,isAdmin:resulto.isAdmin,superAdmin:resulto.superAdmin,roleType:resulto.roleType});
      }

    },function(erro) {
      console.log('error isadmin');
      console.log(erro);
      if(proceed) {
        response.error(erro);
        proceed=false;
      }

    });
});

Parse.Cloud.define('getlinkedinData', function (request, response) {
  if (!request.user) {
    return response.error('Must be logged in.');
  }
  var query = new Parse.Query(TokenStorage);
  query.equalTo('user', request.user);
  query.ascending('createdAt');
  Parse.Promise.as().then(function () {
    return query.first({useMasterKey: true});
  }).then(function (tokenData) {
    if (!tokenData) {
      return Parse.Promise.error('No linkedin data found.');
    }
    return getlinkedinUserDetails(tokenData.get('accessToken'));
  }).then(function (userDataResponse) {
    var userData = userDataResponse.data;
    response.success(userData);
  }, function (error) {
    response.error(error);
  });
});
var getlinkedinAccessToken = function (code) {
  var grantType = "authorization_code";
  var redirectUri = "https://app.hybridrecruitment.com/oauthCallback";

  var body = querystring.stringify({
    grant_type: grantType,
    client_id: linkedinClientId,
    client_secret: linkedinClientSecret,
    code: code,
    redirect_uri: redirectUri
  });
  return Parse.Cloud.httpRequest({
    method: 'POST',
    url: linkedinValidateEndpoint,
    headers: {
      'Accept': 'application/json',
      'User-Agent': 'Parse.com Cloud Code'
    },
    body: body
  });
}

var getlinkedinUserDetails = function (accessToken) {
  return Parse.Cloud.httpRequest({
    method: 'GET',
    url: linkedinUserEndpoint,
    params: {
      oauth2_access_token: accessToken,
      format: 'json'
    },
    headers: {
      'User-Agent': 'Parse.com Cloud Code',
      'Content-Type': 'application/json'
    }
  });
}

var upsertlinkedinUser = function (accessToken, linkedinData) {
  var query = new Parse.Query(TokenStorage);
  query.equalTo('linkedinId', linkedinData.id);
  query.ascending('createdAt');
  var password;
  // Check if this linkedinId has previously logged in, using the master key
  return query.first({useMasterKey: true}).then(function (tokenData) {
    // If not, create a new user.
    if (!tokenData) {
      return newlinkedinUser(accessToken, linkedinData);
    }
    // If found, fetch the user.
    var user = tokenData.get('user');
    return user.fetch({useMasterKey: true}).then(function (user) {
      // Update the accessToken if it is different.
      if (accessToken !== tokenData.get('accessToken')) {
        tokenData.set('accessToken', accessToken);
      }
      /**
       * This save will not use an API request if the token was not changed.
       * e.g. when a new user is created and upsert is called again.
       */
      return tokenData.save(null, {useMasterKey: true});
    }).then(function (obj) {
      password = new Buffer(24);
      _.times(24, function (i) {
        password.set(i, _.random(0, 255));
      });
      password = password.toString('base64')
      user.setPassword(password);
      return user.save();
    }).then(function (user) {
      return Parse.User.logIn(user.get('username'), password);
    }).then(function (user) {
      // Return the user object.
      return Parse.Promise.as(user);
    });
  });
}


var newlinkedinUser = function (accessToken, linkedinData) {
  var user = new Parse.User();
  // Generate a random username and password.
  var username = new Buffer(24);
  var password = new Buffer(24);
  _.times(24, function (i) {
    username.set(i, _.random(0, 255));
    password.set(i, _.random(0, 255));
  });
  user.set("linkedIn", true);

  user.set("username", username.toString('base64'));
  user.set("password", password.toString('base64'));

  user.set("firstname", linkedinData.firstName);
  user.set("lastname", linkedinData.lastName);
  user.set("email", linkedinData.email);
  user.set("profileImgURL", linkedinData.pictureUrl);


  // Sign up the new User
  return user.signUp().then(function (user) {
    // create a new TokenStorage object to store the user+linkedin association.
    var ts = new TokenStorage();
    ts.set('linkedinId', linkedinData.id);
    ts.set('linkedinFirstname', linkedinData.firstName);
    ts.set('linkedinLastname', linkedinData.lastName);
    ts.set('accessToken', accessToken);
    ts.set('user', user);
    ts.setACL(restrictedAcl);
    // Use the master key because TokenStorage objects should be protected.
    return ts.save(null, {useMasterKey: true});
  }).then(function (tokenStorage) {
    return upsertlinkedinUser(accessToken, linkedinData);
  });
}


/* end linkedin */


pushUrlPrefixes.forEach(function (path) {
  app.get("/" + path + "*", getIndex(function (req, res) {
    console.log("GET TOTAL ***************");

    console.log(req);
    res.set('Content-Type', 'text/html');
    res.status(200).send(index);
  }));
});


//redirect requests here, instead of parse hosting
app.listen();
