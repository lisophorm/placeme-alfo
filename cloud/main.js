require('cloud/server.js');
require('cloud/jobs.js');

// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function (request, response) {
  response.success("Hello world!");
});

Parse.Cloud.afterSave('Portfolio', function (request, response) {
  try {
    var authorUser=Parse.User.current().id;
  } catch(e) {
    var authorUser='undefined';
  }
  Parse.Cloud.run('saveLog',{'action':'Save Portfolio',
    'author':authorUser,
    'detail':JSON.stringify(request.object)});
  Parse.Cloud.useMasterKey();
  var setting = request.object;

  if (!setting.existed()) {
    setting.set('user', Parse.User.current());

    var relation = setting.relation("owner");
    relation.add(Parse.User.current());
    var newACL = new Parse.ACL(Parse.User.current());
    newACL.setPublicReadAccess(false);
    newACL.setRoleWriteAccess("Administrator", true);
    newACL.setRoleReadAccess("Administrator", true);
    newACL.setWriteAccess(Parse.User.current(), true);
    newACL.setReadAccess(Parse.User.current(), true);

    setting.setACL(newACL);
    setting.save().then(function (image) {
      console.log('Portfolio salvato');
      console.log(image);
    }, function (error) {
      console.log('portfolio errorazzo');
      response.error(error);
      console.log(error);
    });
  }
})
Parse.Cloud.beforeSave('Clients', function (request, response) {

    var newEntryClient = request.object;
    var keys = Object.keys(request.object.toJSON());
    console.log('company keys');
    console.log(keys);
    var queryClients = new Parse.Query('Clients');
    queryClients.equalTo("name", newEntryClient.get("name"));

    // this could be a sort of signature for your Client, to make more unique (skipping spaces and new lines for example)
    queryClients.first().then(
      function (object) {
        if (typeof object !== 'undefined') {
          console.log("********** CLIENT EXISTIBNG");

          if (object && object.has('linkedinID')) {
            console.log("********** NOTHNG DONE");
            response.error({errorCode: 123, errorMsg: "Client already exist!"});
          } else {
            object.destroy().then(function (resp) {
              console.log("********** DESTROYED OLD ENTRY");
              response.success();
            }, function (err) {
              console.log("********** ERR DONE");
              console.log(err);
              response.success();
            });

          }
        } else {
          response.success();
        }

      },
      function (error) {
        response.error("Could not validate uniqueness for this Id object.");

      });
  }
);

var SECRET = "6LdA5xETAAAAAC3AHUcviAji8DDfH1grCNKkX1YW";
Parse.Cloud.beforeSave(Parse.User, function (request, response) {
  Parse.Cloud.useMasterKey();
  var authData = request.object.get('authData');
  console.log('auth data');
  console.log(typeof authData);
  if (typeof authData == "undefined") {
    console.log('authdata indefinito');
  }


  if (request.object.get('linkedIn') && request.object.isNew()) {
    console.log('USER FROM LINKEDIN!');
    response.success();

  } else if (request.object.get('backend') && request.object.isNew()) {
    request.object.unset('backend');
    console.log('USER FROM BACKEND!');
    response.success();

  }
  if (request.object.isNew()) {
    var newACL = new Parse.ACL(Parse.User.current());

    newACL.setPublicReadAccess(false);
    //newACL.setRoleWriteAccess("Administrator", true);
    newACL.setRoleReadAccess("Administrator", true);

    request.object.setACL(newACL);

    console.log('NEW USER BEFORESAVE');
    if (typeof authData !== "undefined") {
      console.log('authdata exists');
      if (typeof authData.facebook !== "undefined") {
        response.success();
      }
    }
    Parse.Cloud.httpRequest({
      method: 'GET',
      url: 'https://www.google.com/recaptcha/api/siteverify?secret=' + SECRET + '&response=' + request.object.get('captchares'),
      success: function (httpResponse) {
        console.log('resonse from google');

        //console.log(httpResponse.text);
        var fromGoog = JSON.parse(httpResponse.text);
        if (fromGoog.success == true) {
          request.object.unset('captchares');
          response.success();
        } else {
          response.error(JSON.stringify({code: 666, message: fromGoog['error-codes']}));
          //response.error("Invalid captcha:<br/>"+);
        }

        //response.error("RESPONSE FROM GOOGLE");
      },
      error: function (httpResponse) {
        //console.log('error');
        //console.log(httpResponse);

        response.error("Error from Google Re-captcha");
        //response.error("error calling google");
      }
    });

  } else {
    console.log('USER EXISTED BEFORESAVE');
    response.success();
  }
  //if (!request.object.get("email")) {

  // } else {
  // response.success();
  //}
});

Parse.Cloud.afterSave(Parse.User, function (request, response) {
  try {
    var authorUser=Parse.User.current().id;
  } catch(e) {
    var authorUser='undefined';
  }
  Parse.Cloud.run('saveLog',{'action':'Save User Object',
    'author':authorUser,
    'detail':JSON.stringify(request.object)});

  console.log('cazz' + request.object.id);
  console.log(request.object.id);
  //Parse.Cloud.useMasterKey();
  var user = request.object;
  var createdAt = request.object.get("createdAt");
  var updatedAt = request.object.get("updatedAt");
  var objectExisted = (createdAt.getTime() != updatedAt.getTime());
//request.object.isNew()
  if (objectExisted) {
    console.log("utente esisteva!");
    return;
  } else {
    console.log("utente non esisteva!");
    Parse.Cloud.useMasterKey();
    var user = request.object;


    var RestrictedUserInfo = Parse.Object.extend("RestrictedUserInfo");
    var restrictedUserInfo = new RestrictedUserInfo();
    var newACL = new Parse.ACL();
    newACL.setPublicReadAccess(false);
    newACL.setRoleWriteAccess("Administrator", true);
    newACL.setRoleReadAccess("Administrator", true);
    restrictedUserInfo.setACL(newACL);
    restrictedUserInfo.set('userID', Parse.User.current());
    restrictedUserInfo.save();

    return;


  }


});

String.prototype.cleanup = function () {
  //keeps only characters
  var temp = this.toLowerCase().replace(/[^a-zA-Z0-9]+/g, " ");
  //now removes messy double spaces
  return temp.replace(/\s\s/g, ' ');
}

var _ = require('underscore');
Parse.Cloud.beforeSave("UserInfo", function (request, response) {
  var post = request.object;
  var words = [];

  var toLowerCase = function (w) {
    return w.toLowerCase();
  };
  if (post.get("jobtitle")) {
    words = words.concat(post.get("jobtitle").cleanup().split(/\b\s+(?!$)/));
  }
  if (post.get("biography")) {
    words = words.concat(post.get("biography").cleanup().split(/\b\s+(?!$)/));
  }
  if (post.get("firstname")) {
    words.push(post.get("firstname"));
  }
  if (post.get("lastname")) {
    words.push(post.get("lastname"));
  }
  if (post.get("middlename")) {
    words.push(post.get("middlename"));
  }
  if (post.get("email")) {
    words.push(post.get("email"));
  }
  if (post.get("location")) {
    words = words.concat(post.get("location").split(/\b\s+(?!$)/));
  }
  if (post.get("skilltags")) {
    words = words.concat(post.get("skilltags"));
  }
  if (post.get("notes")) {
    words = words.concat(post.get("notes").split(/\b\s+(?!$)/));
  }

  words = _.map(words, toLowerCase);

  //var stopWords = ["the", "in", "and"];
  //words = _.filter(words, function(w) { return w.match(/^w+$/) && ! _.contains(stopWords, w); });
  /*var newACL = new Parse.ACL(Parse.User.current());
   newACL.setPublicReadAccess(false);
   newACL.setPublicWriteAccess(false);
   newACL.setRoleWriteAccess("Administrator", true);
   newACL.setRoleReadAccess("Administrator", true);
   post.setACL(newACL);*/

  post.set("words", words);
  response.success();
});

Parse.Cloud.afterSave("UserInfo", function (request, response) {
  try {
    var authorUser=Parse.User.current().id;
  } catch(e) {
    var authorUser='undefined';
  }
  Parse.Cloud.run('saveLog',{'action':'Save User Info',
    'author':authorUser,
    'detail':JSON.stringify(request.object)});
  var oggetto = request.object;
  if (oggetto.has('userID')) {
    console.log('user is alive');
    Parse.Cloud.useMasterKey();
    var currentUser = oggetto.get('userID');
    if (oggetto.has('email')) {
      currentUser.setUsername(oggetto.get('email'));
      currentUser.setEmail(oggetto.get('email'));
    }
    if (oggetto.has('firstname')) {
      currentUser.set('firstname', oggetto.get('firstname'));
    }
    if (oggetto.has('lastname')) {
      currentUser.set('lastname', oggetto.get('lastname'));
    }
    if (oggetto.has('profileImgURL')) {
      currentUser.set('profileImgURL', oggetto.get('profileImgURL'));
    }
    if (oggetto.has('publicProfileUrl')) {
      currentUser.set('publicProfileUrl', oggetto.get('publicProfileUrl'));
    }

    currentUser.unset('captchares');
    if (oggetto.has('newFromBackend')==false) {
      console.log('old object');
      currentUser.save().then(function (success) {
        console.log(success);
      }, function (error) {
        console.log(error);
      });
    }else {
      console.log('usernewFromBackend');
      oggetto.unset('newFromBackend');
      oggetto.save().then(function (success) {
        console.log('usernewFromBackend removed');
        console.log(success);
      }, function (error) {
        console.log('usernewFromBackend error');
        //console.log('usernewFromBackend');
        console.log(error);
      })
    }
  }
  console.log('aftersave userInfo' + oggetto.id);
});

/* all the ACL STUFF */
Parse.Cloud.define('assignACL', function (req, response) {
  var continues = true;
  var currentRole = {};
  var currentUser = {};
  var palla = [];
  console.log('current user');
  console.log(req.user);
  var currentUser = {};
  var utente = new Parse.Object("_User");
  utente.id = req.params.userID;

  Parse.Cloud.useMasterKey();
  utente.fetch().then(function (result) {
    console.log('FETCHED USER');
    currentUser = result;
    var checlRoles = new Parse.Query('_Role');
    checlRoles.equalTo('users', utente);
    Parse.Cloud.useMasterKey();
    return checlRoles.find();
  }, function (error) {
    console.log('error check roles');
    console.log(error);
  }).then(function (result) {
      console.log('found roles');
      console.log(result.length);
      if (result.length > 0) {
        for (var i = 0; i < result.length; i++) {
          result[i].get('users').remove(currentUser);
          console.log('get users');
          console.log(result[i].getUsers());
          console.log('***********');
        }
        return Parse.Object.saveAll(result);
      }
      return true;
    }, function (error) {
      console.log('error roles');
      console.log(error.length);
    }
  ).then(function (utente) {
    if (typeof utente !== 'undefined') {
      console.log('found UTENTO');
      Parse.Cloud.useMasterKey();
      var queryCode = new Parse.Query('_Role');
      queryCode.equalTo('name', req.params.clientID + '_' + req.params.role);
      return queryCode.first();
    } else {
      if (continues) {
        continues = false;
        response.error('no user found for ID ' + req.params.userID);
      }
    }
  }, function (errUtente) {
    if (continues) {
      continues = false;
      response.error(errUtente);
    }
  }).then(function (ruolo) {
    if (typeof ruolo !== 'undefined') {
      var currentRole = ruolo.get('roles');
      ruolo.getUsers().add(currentUser);
      console.log('found RUOLO');
      currentUser.set('ACLClientID', req.params.clientID);
      currentUser.set('ACLLevel', req.params.role);
      return ruolo.save();

    } else {
      if (continues) {
        continues = false;
        response.error('no ROLE found for ID ' + req.params.clientID + '_' + req.params.role);

      }
    }
  }, function (errUtente) {
    if (continues) {
      continues = false;
      response.error(errUtente);
    }
  }).then(function (salvatutti) {
    return currentUser.save();
  }, function (errRisosto) {
    if (continues) {
      continues = false;
      response.error(errRisosto);
    }
  }).then(function (salvatutti1) {
    response.success('viva Maria Vergine');
  }, function (errRisosto1) {
    if (continues) {
      continues = false;
      response.error(errRisosto1);
    }
  });
  ;
});

Parse.Cloud.beforeSave("ClientAdmins", function (request, response) {
  var post = request.object;
  var incomplete = true;

  if (request.object.isNew()) {
    console.log('new client');
    console.log(post.id);
    var newACL = new Parse.ACL();
    newACL.setPublicReadAccess(false);
    newACL.setPublicWriteAccess(false);
    newACL.setRoleWriteAccess("Administrator", true);
    newACL.setRoleReadAccess("Administrator", true);
    post.setACL(newACL);
  } else {
    console.log('Old Client');
    console.log(post.id);
  }
  //var stopWords = ["the", "in", "and"];
  //words = _.filter(words, function(w) { return w.match(/^w+$/) && ! _.contains(stopWords, w); });
  /**/

  if (incomplete) {
    response.success();
  }


});
Parse.Cloud.afterSave("_Role", function (request, response) {
  console.log('aftersave ROLES');
});

Parse.Cloud.afterSave("ClientAdmins", function (request, response) {
  var post = request.object;
  console.log('aftersave ClientAdmins');
  if (request.object.existed()) {
    console.log('client existed client');
    console.log(post.id);
    console.log(post.get('name'));

  } else {
    console.log('Client was new');
    console.log(post.id);
    console.log(post.get('name'));
    //createRolesForOrganization(post.id,post.get('name'));
  }

  Parse.Cloud.useMasterKey();
  var gino = new Parse.Query('_Role');
  gino.equalTo('clientId', post.id);
  gino.first().then(function (oggerro) {
    if (typeof oggerro !== 'undefined') {
      console.log('TROVATOOOO' + oggerro.id);
      console.log(oggerro);
    } else {
      createRolesForOrganization(post.id, post.get('name')).then(function (gino) {
        console.log('mamma;');
      }, function (erroro) {
        console.log('mamma err');
        console.log(erroro);
      });
      console.log('NOOO TROVATOOOO');
    }
  }, function (errore) {
    console.log(errore);
  });

});


function createRolesForOrganization(orgID, clientName) {
  console.log('create roles');
  var promise = new Parse.Promise();
  console.log(orgID);
  console.log(clientName);
  Parse.Cloud.useMasterKey();
  var self = Parse.User.current();
  var ownerRole = new Parse.Role(orgID + '_Owner', new Parse.ACL(self)),
    adminRole = new Parse.Role(orgID + '_Admin', new Parse.ACL(self)),
    userRole = new Parse.Role(orgID + '_User', new Parse.ACL(self));

  //ownerRole.getRoles().add('Administrator');
  // adminRole.getRoles().add('Administrator');
  // userRole.getRoles().add('Administrator');

  ownerRole.set('clientName', clientName);
  ownerRole.set('clientId', orgID);
  ownerRole.set('roleType', 'Owner');
  adminRole.set('clientName', clientName);
  adminRole.set('clientId', orgID);
  adminRole.set('roleType', 'Admin');
  userRole.set('clientName', clientName);
  userRole.set('clientId', orgID);
  userRole.set('roleType', 'User');

  return Parse.Object
    .saveAll([
      ownerRole,
      adminRole,
      userRole
    ])
    .then(function (objs) {
      ownerRole = objs[0];
      adminRole = objs[1];
      userRole = objs[2];

      userRole.getRoles().add(adminRole);
      adminRole.getRoles().add(ownerRole);
      //ownerRole.getRoles().add('Administrator');
      return Parse.Object.saveAll([
        ownerRole,
        adminRole,
        userRole
      ]);
    });
  return promise;
}


Parse.Cloud.beforeSave("SigninDocuments", function (request, response) {
  var post = request.object;
  if (post.has('signerName')) {
    post.set('signerName', post.get('signerName').toLowerCase());
  }
  if (post.has('signerRole')) {
    post.set('signerRole', post.get('signerRole').toLowerCase());
  }
  if (post.has('signerID')) {
    var utente = new Parse.Object("_User");
    utente.id = post.get('signerID');
    console.log('signdocuments aftersave');
    Parse.Cloud.useMasterKey();
    utente.fetch().then(function (utento) {
      var newACL = new Parse.ACL(Parse.User.current());
      newACL.setPublicReadAccess(false);
      newACL.setPublicWriteAccess(false);
      if (utento.has('ACLClientID')) {
        console.log('this utento job bag ACL');
        var currId = utento.get('ACLClientID');
        newACL.setRoleWriteAccess(currId + '_Owner', true);
        newACL.setRoleReadAccess(currId + '_Owner', true);
        newACL.setReadAccess(utento.id, true);
        newACL.setWriteAccess(utento.id, true);
      }
      newACL.setRoleWriteAccess("Administrator", true);
      newACL.setRoleReadAccess("Administrator", true);
      post.setACL(newACL);
      response.success();
    }, function (error) {
      console.log('error fetching user timesheet');
      response.error(error);
    })
  } else {
    if (request.object.isNew() && post.get('docType') == 'timesheet') {
      console.log('SET ACL for TIMESHEET')
      var newACL = new Parse.ACL(Parse.User.current());
      newACL.setPublicReadAccess(false);
      newACL.setPublicWriteAccess(false);
      if (Parse.User.current().has('ACLClientID')) {
        console.log('this is a client job bag ACL');
        var currId = Parse.User.current().get('ACLClientID');
        newACL.setRoleWriteAccess(currId + '_Owner', true);
        newACL.setRoleReadAccess(currId + '_Owner', true);
        newACL.setReadAccess(object.id, true);
        newACL.setWriteAccess(object.id, true);
      }
      newACL.setRoleWriteAccess("Administrator", true);
      newACL.setRoleReadAccess("Administrator", true);
      post.setACL(newACL);
      response.success();
    } else {
      response.success();
    }
  }


});

Parse.Cloud.afterSave("SigninDocuments", function (request, response) {
  try {
    var authorUser=Parse.User.current().id;
  } catch(e) {
    var authorUser='undefined';
  }
  Parse.Cloud.run('saveLog',{'action':'SigninDocuments',
    'author':authorUser,
    'detail':JSON.stringify(request.object)});
  var req = request.object;



  if (req.get('docType') != 'timesheet') {
    //console.log(object);
    var utente = new Parse.Object("_User");
    utente.id = req.get('userID');
    console.log('signdocuments aftersave');
    Parse.Cloud.useMasterKey();
    utente.fetch().then(function (object) {

      console.log('fetched utente');
      console.log(object);
      Parse.Cloud.useMasterKey();
      var newACL = new Parse.ACL();
      newACL.setPublicReadAccess(false);
      newACL.setPublicWriteAccess(false);
      if (object.has('ACLClientID')) {
        console.log('this is a client job bag ACL');
        var currId = object.get('ACLClientID');
        newACL.setRoleWriteAccess(currId + '_Owner', true);
        newACL.setRoleReadAccess(currId + '_Owner', true);
        newACL.setReadAccess(object.id, true);
        newACL.setWriteAccess(object.id, true);
      }
      newACL.setRoleWriteAccess("Administrator", true);
      newACL.setRoleReadAccess("Administrator", true);
      req.setACL(newACL);
      req.save();

    }, function (error) {
      console.log(error);

    })
  }
});


Parse.Cloud.beforeSave('WorkAttachments', function (request, response) {
  var coso = request.object.get('attach');
  Parse.Cloud.useMasterKey();
  var relation = request.object.relation("userLink");
  relation.add(Parse.User.current());
  var newACL = new Parse.ACL(Parse.User.current());
  //newACL.setPublicReadAccess(false);
  newACL.setRoleWriteAccess("Administrator", true);
  newACL.setRoleReadAccess("Administrator", true);
  request.object.setACL(newACL);

  console.log('COSO');
  console.log(coso);
  response.success();
});
Parse.Cloud.beforeSave('JobBag', function (request, response) {

  if (request.object.isNew()) {
    console.log("SONO NUOVO AL MONDO");
    var queryCode = new Parse.Query('JobBagProg');
    queryCode.first().then(function (obj) {
      obj.set('count', obj.get('count') + 1);
      return obj.save();
    }, function (error) {
      response.error(error);
    }).then(function (obj) {
      response.success();
    }, function (error) {
      response.error(error);
    });
  } else {
    response.success();
  }


});
Parse.Cloud.afterSave('JobBag', function (request, response) {
  try {
    var authorUser=Parse.User.current().id;
  } catch(e) {
    var authorUser='undefined';
  }
  Parse.Cloud.run('saveLog',{'action':'Save Job Bag',
    'author':JSON.stringify(request.user),
    'detail':JSON.stringify(request.object),
    'debug':JSON.stringify(request)});
  var utente = new Parse.Object("_User");
  utente.id = Parse.User.current().id;
  console.log('User from AFTERSAVE Job Bag');
  console.log(utente);

  utente.fetch().then(function (successo) {
    console.log('Job basg user fetched');
    Parse.Cloud.useMasterKey();
    var newACL = new Parse.ACL(request.user);
    if (successo.has('ACLClientID')) {
      console.log('this is a client job bag ACL');
      var currId = successo.get('ACLClientID');
      newACL.setRoleWriteAccess(currId + '_Owner', true);
      newACL.setRoleReadAccess(currId + '_Owner', true);
      //newACL.setReadAccess(currId, true);
      //newACL.setWriteAccess(currId, true);
    }
    newACL.setPublicReadAccess(false);
    newACL.setPublicWriteAccess(false);
    newACL.setRoleWriteAccess("Administrator", true);
    newACL.setRoleReadAccess("Administrator", true);
    request.object.setACL(newACL);
    request.object.save();
  });
});

Parse.Cloud.beforeDelete('UserInfo', function (request, response) {
  if (request.object.isNew()) {
    console.log("SONO NUOVO AL MONDO");
    var queryCode = new Parse.Query('JobBagProg');
    queryCode.first().then(function (obj) {
      obj.set('count', obj.get('count') + 1);
      return obj.save();
    }, function (error) {
      response.error(error);
    }).then(function (obj) {
      response.success();
    }, function (error) {
      response.error(error);
    });
  } else {
    response.success();
  }


});

var isAdmin = function () {
  var promise = new Parse.Promise();

}
Parse.Cloud.define('saveLog', function (req, response) {
  var LogFile = Parse.Object.extend("LogFile");
  var logFile = new LogFile();
  logFile.set('action',req.params.action);
  logFile.set('authorID',req.params.author);
  logFile.set('detail',req.params.detail);
  logFile.set('debug',req.params.debug);
  logFile.save().then(function(success) {
    response.success();
  },function(response) {
    response.error(error);
  })
});
Parse.Cloud.define('isAdmin', function (req, response) {
  //if(!req.params.username){
  //     response.error('Username has not been provided');
  // }
  console.log('isadmin req.params.currentUser');
  console.log(req.params);
  var queryRole = new Parse.Query(Parse.Role);
  queryRole.equalTo('name', 'Administrator');
  if (req.params.currentUser) {
    var utente = new Parse.Object("_User");
    utente.id = req.params.currentUser;
    console.log('GOT USER FROM CALL');
  } else {
    var utente = Parse.User.current();
    console.log('GOT USER FROM CURRENT');
  }

  utente.fetch().then(function (successo) {
    console.log('fetched utente inside isadmin');
    var privileges = {};
    if (successo.has('ACLLevel')) {
      if (successo.get('ACLLevel') == 'Admin' || successo.get('ACLLevel') == 'Owner') {
        console.log("********* this is client admin");
        privileges.isAdmin = true;
        privileges.roleType = successo.get('ACLLevel');
      } else {
        console.log("********* this is NOT admin");
        privileges.isAdmin = false;
        privileges.roleType = 'Candidate';
      }
    }
    queryRole.first({
      success: function (r) {
        var role = r;
        var relation = new Parse.Relation(role, 'users');
        var admins = relation.query();

        admins.equalTo('email', successo.get('email'));
        admins.first({
          success: function (u) {
            var user = u;

            if (user) {
              response.success({'superAdmin': true, 'isAdmin': true, 'roleType': 'SuperAdmin'});
            } else {

              console.log('signer email');
              response.success({'superAdmin': false, 'isAdmin': privileges.isAdmin, 'roleType': privileges.roleType});
            }
          },
          error: function () {
            response.error('Error on user lookup');
          }
        })
      },
      error: function () {
        response.error('User admin check failed');
      }
    });
  });


});


var signPassword = 'k0st0golov';
var signApikey = '939B2CFD3EC41BA954FE4CEA8C169D7DFC7D06959BD1D574E14641BB6785DD07';
var signcCient_id = 'HybridRecruitment';
var signTemplatename = 'template02';
var signMasterUser = 'lisophorm@gmail.com';

Parse.Cloud.define('getSignToken', function (req, response) {
  var signerEmail = '';
  if (typeof req.params.email === 'undefined') {
    console.log('MASTEE USER');
    signerEmail = signMasterUser;
  } else {
    signerEmail = req.params.email;
  }
  Parse.Cloud.httpRequest({
    url: 'https://api.signinghub.com/authenticate',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    },
    body: 'grant_type=password&client_id=' + signcCient_id
    + '&client_secret=' + signApikey + '&username='
    + signerEmail + '&password='
    + signPassword
  }).then(function (httpResponse) {
      console.log('got the token cloud');
      console.log(httpResponse.data); // Return Parse File info
      response.success({access_token: httpResponse.data.access_token});
    },
    function (httpResponse) {
      console.log('Failed with: ' + httpResponse.status);
      console.log(httpResponse);
      response.error(httpResponse.text);
    })
});

Parse.Cloud.define('recoverSignDocument', function (req, response) {
  console.log('result 2');
  Parse.Cloud.useMasterKey();

  var signerEmail = '';
  if (typeof req.params.email === 'undefined') {
    console.log('MASTEE USER');
    signerEmail = signMasterUser;
  } else {
    signerEmail = req.params.email;
  }
  Parse.Cloud.httpRequest({
    url: 'https://api.signinghub.com/authenticate',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    },
    body: 'grant_type=password&client_id=' + signcCient_id
    + '&client_secret=' + signApikey + '&username='
    + signerEmail + '&password='
    + signPassword
  }).then(function (httpResponse) {
      console.log('got the token cloud');
      console.log(httpResponse.data); // Return Parse File info
      response.success({access_token: httpResponse.data.access_token});
    },
    function (httpResponse) {
      console.log('Failed with: ' + httpResponse.status);
      console.log(httpResponse);
      response.error(httpResponse.text);
    })
});


Parse.Cloud.define('retrieveSignDocument', function (req, response) {
  Parse.Cloud.useMasterKey();
  var docObj = 0;
  var docu_signer = '';
  console.log('Retrieve sign document' + req.params.pdfID);
  console.log(JSON.stringify(req.params));
  console.log('********');
  if (typeof req.params !== 'undefined') {
    console.log('******** params SI');
    var thePDF = req.params.pdfID.toString();
  } else {
    var thePDF = req.params.pdfID.toString();
    console.log('******** params NO');
  }


  //var SIgnTimeSheet = Parse.Object.extend("SigninDocuments");
  var myquery = new Parse.Query('SigninDocuments');
  myquery.equalTo('objectId', req.params.pdfID);
  console.log('prima della query ' + req.params.pdfID);
  myquery.first().then(function (gotDoc) {
      docObj = gotDoc;
      if (req.params.isAdmin && docObj.get('docType') == 'contract') {
        docu_signer = docObj.get('authorEmail');
      } else {
        docu_signer = docObj.get('signerEmail');
      }
      console.log('signer is ');
      console.log(docu_signer);
      console.log('document is');
      console.log(docObj.get('signID'));
      return Parse.Cloud.httpRequest({
        url: 'https://api.signinghub.com/authenticate',
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json'
        },
        body: 'grant_type=password&client_id=' + signcCient_id
        + '&client_secret=' + signApikey + '&username='
        + docu_signer + '&password='
        + signPassword
      })
    }, function (errozzo) {
      throw errozzo;
    })
    .then(function (httpResponse) {
        console.log('got the token cloud');
        //console.log(httpResponse); // Return Parse File info
        var responsestring = 'https://web.signinghub.com/Integration?access_token=' + httpResponse.data.access_token + '&document_id=' + docObj.get('signID');
        console.log(responsestring);
        response.success(responsestring);
      },
      function (erroro) {
        console.log('Failed with: ');
        console.log(req.params);

        response.error("Auth error retrieving document " + thePDF);

      });
});

Parse.Cloud.define('signHubUser', function (req, response) {
  console.log('sign hub user');

  Parse.Cloud.httpRequest({
    url: 'https://api.signinghub.com/v2/enterprise/users',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + req.params.access_token,
    },
    body: {
      'user_email': req.params.email,
      'user_name': req.params.name,
      'account_type': 'PERSONAL',
      'user_password': 'k0st0golov',
      'security_question': 'first to pay then',
      'security_answer': 'gino',
      enterprise_role: 'Enterprise Users',
      'email_notification': false
    }

  }).then(function (responsa) {
    console.log('sign hub user success');
    response.success(responsa);
  }, function (error) {
    console.log('sign hub user error');
    console.log(error);
    response.error(error);
  });
})

Parse.Cloud.define('getDocumentLog', function (req, response) {
  console.log('GET DOCMENT LOG');
  var docObject = {};
  var more = true;
  var docu_action = '';
  var docu_reason = '';
  var docu_status = '';
  Parse.Cloud.useMasterKey();

  Parse.Cloud.httpRequest({
    url: 'https://api.signinghub.com/authenticate',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    },
    body: 'grant_type=password&client_id=' + signcCient_id
    + '&client_secret=' + signApikey + '&username='
    + signMasterUser + '&password='
    + signPassword
  }).then(function (httpResponse) {
      console.log('got the token cloud');
      console.log(httpResponse); // Return Parse File info
      return Parse.Cloud.httpRequest({
        url: 'https://api.signinghub.com/v2/documents/' + req.params.signHubID + '/log',
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + httpResponse.data.access_token,
        }

      });
    },
    function (erroro) {
      console.log('token Failed with: ');
      throw (erroro);

    }).then(function (risposta) {
    console.log('signin status log');
    console.log('risposta reload');
    var actions = JSON.parse(risposta.text).actions;
    console.log(JSON.parse(risposta.text).document_status);
    docu_status = JSON.parse(risposta.text).document_status;
    console.log('^^^^^^^^^^^^^');
    var no_res = true;
    for (var i = 0; i < actions.length; i++) {
      var action = actions[i];
      console.log('action ' + i);
      console.log(action.action_type);
      if (action.action_type == "SIGNED") {
        no_res = false;
        docu_action = 'signed';
        docu_reason = docu_status;
        console.log('founsd Signed');
        console.log(action.information);
        console.log(action.information.value);
        break;
      }
      if (action.action_type == "DECLINED_DOCUMENT") {
        no_res = false;
        docu_action = 'declined';
        docu_reason = action.information.value;
        console.log('founsd decline');
        console.log(action.information);
        console.log(action.information.value);
        break;
      }
    }
    if (no_res) {
      response.success(risposta);
    } else {
      var SIgnTimeSheet = Parse.Object.extend("SigninDocuments");
      var queryCode = new Parse.Query(SIgnTimeSheet);
      queryCode.equalTo('signID', req.params.signHubID);
      return queryCode.first();
    }
  }, function (orrore) {
    console.log('error retrieving log');
    console.log(orrore);
    throw (orrore);

  }).then(function (oggetto) {
    if (typeof oggetto !== 'undefined') {
      oggetto.set('status', docu_action);
      oggetto.set('reason', docu_reason);
      return oggetto.save();
    }
  }, function (errors) {
    console.log('erroraz');
    console.log(errors);
    throw (errors);
  }).then(function (oggetto) {
    console.log('successoalcesso');
    response.success(true);
  }, function (error1) {
    console.log('errore finale');
    response.error(error);
  });

});

Parse.Cloud.define('deleteSignDocument', function (req, response) {

  var SIgnTimeSheet = Parse.Object.extend("SigninDocuments");
  var queryCode = new Parse.Query(SIgnTimeSheet);
  queryCode.equalTo('objectId', req.params.pdfID);
  queryCode.first().then(function (fondo) {
      if (typeof fondo !== 'undefined') {
        console.log('found the doc');
        return fondo.destroy();
      } else {
        if (prog) {
          console.log('doc not found');
          prog = false;
          response.error('DOcument not found');
        }

      }


    },
    function (httpResponse) {
      console.log('Failed with: ' + httpResponse.status);
      console.log(httpResponse);
      if (prog) {
        prog = false;
        response.error(httpResponse.text);
      }
    }).then(function (httpResponse) {
      console.log('drestroied the DOC');

      return response.success('SMASHED IT');

    },
    function (httpResponse) {
      console.log('Failed destroy doc with: ' + httpResponse.status);
      console.log(httpResponse);
      if (prog) {
        prog = false;
        return response.error(httpResponse.text);
      }
    })
});


Parse.Cloud.define('finalizeTimesheet', function (req, response) {
  console.log("Finalize timesheet");
  console.log(req.params.pdfID);
  var docRecord = {};
  Parse.Cloud.useMasterKey();

  var SigninDocs = Parse.Object.extend("SigninDocuments");

  var findDoc = new Parse.Query(SigninDocs);
  findDoc.equalTo('objectId', req.params.pdfID)
  findDoc.first().then(function (success) {
    docRecord = success;
    /* SEND EMAIL */
    console.log('sending email');
    console.log(docRecord.get('signerEmail'));
    console.log('sending email to:' + docRecord.get('signerEmail'));
    var sendGrid = require("sendgrid");
    sendGrid.initialize("hybridrecruitment", "k0st0golov");
    var email = sendGrid.Email({to: [docRecord.get('signerEmail')]});
    email.setFrom(docRecord.get('authorEmail'));
    email.setFromName(docRecord.get('authorEmail'));
    // email.addTo('lisophorm@gmail.com');
    email.setHTML('<b>Please review and sign my timesheet: (document ID ' + docRecord.id + ')</b> ');
    email.addFilter('templates', 'template_id', '3f4749a8-afd8-45f5-aabd-08069b304358');
    email.addUniqueArg('signDocID', docRecord.id);
    //email.header.sub=subs;

    email.addSubstitution("-documentID-", docRecord.id);
    email.setSubject('Timesheet from ' + docRecord.get('authorName'));
    console.log('email section');


    return sendGrid.send(email);

    /* end send EMAIL */
  }, function (error) {
    response.error(error);
  }).then(function (resolo) {
    console.log('email success');
    docRecord.set('status', 'email sent');
    docRecord.set('signID', req.params.docID);
    return docRecord.save();
  }, function (errorolo) {
    response.error(errorolo);
    return docRecord.save();
  }).then(function (succo) {
    console.log('end of the line');
    response.success(true);
  }, function (erroz) {
    console.log('error saving obhect');
    response.error(erroz);
  })


});

Parse.Cloud.define('finalizeContract', function (req, response) {
  console.log("Finalize Contract signer 1");
  console.log(req.params.signer1);
  console.log("Finalize Contract signer 2");
  console.log(req.params.signer2);
  var keepGoing = true;
  Parse.Cloud.useMasterKey();

  var SigninDocs = Parse.Object.extend("SigninDocuments");
  var signedDoc = new SigninDocs();

  var newACL = new Parse.ACL();
  newACL.setReadAccess(req.params.signer1.id, true);
  newACL.setReadAccess(req.params.signer2.id, true);
  newACL.setWriteAccess(req.params.signer1.id, true);
  newACL.setWriteAccess(req.params.signer2.id, true);
  newACL.setPublicReadAccess(false);
  newACL.setPublicWriteAccess(false);
  newACL.setRoleWriteAccess("Administrator", true);
  newACL.setRoleReadAccess("Administrator", true);
  signedDoc.setACL(newACL);
  signedDoc.set('userID', req.params.userID);

  signedDoc.set('docType', 'contract');
  signedDoc.set('authorName', req.params.signer1.name);
  signedDoc.set('authorEmail', req.params.signer1.email);
  signedDoc.set('signerName', req.params.signer2.name);
  signedDoc.set('signerEmail', req.params.signer2.email);
  signedDoc.set('signID', req.params.signID);
  signedDoc.set('projectTitle', req.params.title);
  signedDoc.set('status', 'Email sent');

  signedDoc.save().then(function (result) {
    /* SEND EMAIL */
    console.log('sending email');
    //console.log(docRecord.get('signerEmail'));
    console.log('sending email to:' + req.params.signer2.email);
    var sendGrid = require("sendgrid");
    sendGrid.initialize("hybridrecruitment", "k0st0golov");
    var email = sendGrid.Email({to: [req.params.signer2.email]});
    email.setFrom(req.params.signer1.email);
    email.setFromName(req.params.signer1.name);
    // email.addTo('lisophorm@gmail.com');
    email.setHTML('<b>Please review and sign this contract:: (document ID ' + req.params.signID + ')</b>');
    email.addFilter('templates', 'template_id', 'b9b87e6e-8b82-4fe7-b699-bf6f123c347d');
    email.addUniqueArg('signDocID', req.params.signID);
    //email.header.sub=subs;

    email.addSubstitution("-documentID-", result.id);
    email.setSubject('Contract from ' + req.params.signer1.name);
    console.log('email section');

    return sendGrid.send(email);

    /* end send EMAIL */
  }, function (fail) {
    keepGoing = false;
    response.error(fail);
  }).then(function (final) {
    if (keepGoing) {
      response.success(final);
    }


  }, function (error) {
    if (keepGoing) {
      response.error(error);
    }

  })


  /*var findDoc = new Parse.Query(SigninDocs);
   findDoc.equalTo('objectId', req.params.pdfID)
   findDoc.first().then(function (success) {
   docRecord = success; */
  /* SEND EMAIL */
  /*   console.log('sending email');
   console.log(docRecord.get('signerEmail'));
   console.log('sending email to:' + docRecord.get('signerEmail'));
   var sendGrid = require("sendgrid");
   sendGrid.initialize("hybridrecruitment", "k0st0golov");
   var email = sendGrid.Email({to: [docRecord.get('signerEmail')]});
   email.setFrom(docRecord.get('authorEmail'));
   email.setFromName(docRecord.get('authorEmail'));
   // email.addTo('lisophorm@gmail.com');
   email.setHTML('<b>Please review and sign my timesheet:</b> ');
   email.addFilter('templates', 'template_id', '3f4749a8-afd8-45f5-aabd-08069b304358');
   email.addUniqueArg('signDocID', docRecord.id);
   //email.header.sub=subs;

   email.addSubstitution("-documentID-", docRecord.id);
   email.setSubject('Timesheet from ' + docRecord.get('authorName'));
   console.log('email section');


   return sendGrid.send(email);
   */
  /* end send EMAIL */
  /* }, function (error) {
   response.error(error);
   }).then(function (resolo) {
   console.log('email success');
   docRecord.set('status', 'email sent');
   docRecord.set('signID', req.params.docID);
   return docRecord.save();
   }, function (errorolo) {
   response.error(errorolo);
   return docRecord.save();
   }).then(function (succo) {
   console.log('end of the line');
   response.success(true);
   }, function (erroz) {
   console.log('error saving obhect');
   response.error(erroz);
   })*/


});


var Buffer = require('buffer').Buffer;
Parse.Cloud.define('authSign', function (req, response) {
  console.log('authsign for' + req.params.email);
  console.log('authsign for' + req.params.pdfID);
  var access_token = '';

  Parse.Cloud.httpRequest({
    url: 'https://api.signinghub.com/authenticate',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    },
    body: 'grant_type=password&client_id=' + signcCient_id
    + '&client_secret=' + signApikey + '&username='
    + signMasterUser + '&password='
    + signPassword
  }).then(function (httpResponse) {
      console.log('got the token cloud');
      console.log('//bottle-crystalbits.rhcloud.com/pdf?url=app.hybridrecruitment.com/timesheet/' + req.params.pdfID);
      //console.log(httpResponse.data); // Return Parse File info
      access_token = httpResponse.data.access_token;

      return Parse.Cloud.httpRequest({
        method: 'GET',
        url: '//bottle-crystalbits.rhcloud.com/pdf?url=app.hybridrecruitment.com/timesheet/' + req.params.pdfID,
        responseType: 'text'
      });
    },
    function (httpResponse) {
      console.log('Failed with: ' + httpResponse.status);
      console.log(httpResponse);
      response.error(httpResponse.text);
    }).then(function (httpImgFile) {
      console.log('buffer file here');
      console.log(httpImgFile);
      console.log('end file here');


      //console.log(httpImgFile.buffer.toString('binary'));
      //var tempFile=new File(new Array(httpImgFile.buffer),'myfile.pdf');
      return Parse.Cloud.httpRequest({
        url: 'https://api.signinghub.com/v2/documents',
        method: 'POST',
        headers: {
          'Content-Type': 'Application/octet-stream',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + access_token,
          'file-name': 'timesheet' + req.params.pdfID + '.pdf',
          'convert-document': 'true',
          'source': 'API'

        }, body: httpImgFile.text
      });
    }
  ).then(function (file) {
    console.log('save to signatyre');
    console.log(file);
  }, function (error) {
    console.log('error parseupload');
    console.log(error);
  })

});

Parse.Cloud.define('getSigners', function (req, response) {
  Parse.Cloud.useMasterKey();

  var query = new Parse.Query('_User');
  query.startsWith("email", req.params.email); // find users that match
  query.exists("ACLLevel");
  query.find().then(function (friend) {
      //console.log("find result" + stringa);
      var resulto = [];
      console.log(friend.length);
      if (friend.length > 0) {
        for (var i = 0; i < friend.length; i++) {
          var object = friend[i];
          var thisOne = new Object({
            id: object.id,
            name: object.get('firstname') + ' ' + object.get('lastname'),
            email: object.get('email'),
            role: object.get('role')
          });

          console.log(thisOne);
          resulto.push(thisOne);
          console.log(resulto);
        }
        console.log("Success");
        console.log(JSON.stringify(resulto));
        response.success(JSON.stringify(resulto));
      } else {
        response.success(JSON.stringify(resulto));
      }
    },
    function (error) {
      //Show if no user was found to match
      response.error(error);

    });
});

Parse.Cloud.define('getCode', function (req, response) {
  var queryCode = new Parse.Query('JobBagProg');
  queryCode.first().then(function (obj) {
    response.success(obj.get('count'))
  }, function (error) {
    response.error(error);
  });
});

Parse.Cloud.define('updateUser', function (req, response) {
  Parse.Cloud.useMasterKey();
  console.log("REQUEST USER ID:" + req.params.userID);
  var queryCode = new Parse.Query('_User');
  queryCode.equalTo('objectId', req.params.userID);
  queryCode.first().then(function (obj) {
    if (typeof obj !== 'undefined') {
      console.log("USER ID:");
      console.log(obj);
      obj.set('firstname', vm.firstname);
      obj.set('lastname', vm.lastname);
      obj.set('email', vm.email);
      obj.set('username', vm.email);
      Parse.Cloud.useMasterKey();
      return obj.save();
    } else {
      console.log("USER IDnonono");
      response.error('no user found');
    }

  }, function (error) {
    console.log("ERRORE QUI");
    console.log(error);
    response.error(error);
  }).then(function (obj2) {
    if (typeof obj2 !== 'undefined') {
      console.log("SUCCESSONE");
      console.log(obj2);
    } else {
      console.log("NO USERAZZO");
      response.error('no user found');
    }

  }, function (error) {
    console.log("ERRIRIBE");
    console.log(error);
    response.error(error);
  });
});

Parse.Cloud.define('updateJobOffer', function (req, response) {
  console.log('updateJobOffer');
  var terminated = true;
  console.log(req.params.candidateID);
  console.log(req.params.response);
  var queryCode = new Parse.Query('JobPeople');
  queryCode.equalTo('objectId', req.params.candidateID);
  queryCode.first().then(function (obj) {
    if (typeof obj !== 'undefined') {
      obj.set('candidateResponse', req.params.response);
      if (typeof req.params.message !== 'undefined') {
        obj.set('candidateMessage', req.params.message);
      }
      return obj.save();
    } else {
      console.log('candidate not found');
      response.error('Candidate not found');
      terminated = false;
    }

  }, function (error) {
    response.error(error);
  }).then(function (obj) {
    if (terminated) {
      response.success('response saved');
    }

  }, function (error) {
    if (terminated) {
      response.error(error);
    }

  });
});


Parse.Cloud.define('grabURL', function (req, response) {
  console.log('ciao graburl');
  console.log(req.params.requestedUrl);
  Parse.Cloud.httpRequest({

    url: req.params.requestedUrl,
    success: function (httpResponse) {
      console.log('success');
      console.log(httpResponse.text);
      response.success(httpResponse.text);
    },
    error: function (httpResponse) {
      console.log('FAIL');
      console.log(httpResponse);
      response.error(httpResponse.status);
      // var failer = new Array();
      //failer[0] = "fail";
      //failer[1] = httpResponse.status;
      //response.success(failer);
    }
  });


});
